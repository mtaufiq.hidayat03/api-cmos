<?php
return [
  'webUrl' => env('WEB_URL', 'https://e-pameran.gasik.id'),
  'kindVehicles' => [
    'car',
    'motorcycle',
    'plane',
    'ship',
    'bus',
    'train',
    'boat',
    'bike',
    'truck',
    'other'
  ],
  'fuels' => [
    'gasoline',
    'diesel',
    'gas',
    'electric',
    'aviation gasoline',
    'aviation kerosine',
    'other'
  ]
];

// [
//   'bensin',
//   'diesel',
//   'gas',
//   'electric',
//   'aviation gasoline',
//   'aviation kerosine',
//   'other'
// ]