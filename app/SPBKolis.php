<?php

namespace App;

class SPBKolis extends BaseModel {
  protected $casts = [];
  protected $table = 'spb_kolis';
  protected $appends = ['bpb_status_humanize', 'qty_frontend'];
  public static $statusActives = ['0', '2'];
  public static $statusDones = ['1'];
  public static $statusProgress = 0;
  public static $statusPartial = 2;
  public static $statusDone = 1;

  public function getBpbStatusHumanizeAttribute() {
    $resultStatus = '';
    switch ($this->bpb_status) {
      case '0':
        $resultStatus = 'Belum Melakukan';
        break;
      case '1':
        $resultStatus = 'Done';
        break;
      case '2':
        $resultStatus = 'Partial';
        break;
      default:
        $resultStatus = 'Tidak Diketahui';
        break;
    }
    return $resultStatus;
  }

  public function getQtyFrontendAttribute() {
    $result = '';
    switch ($this->bpb_status) {
      case '0':
        $result = $this->qty;
        break;
      case '2':
        $result = $this->qty_parsial;
        break;
      default:
        $result = null;
        break;
    }
    return $result;
  }

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function scopeQueryDetail() {
    return $this->with([
      'spb:id,doc_no,location_id',
      'purchaseItem:id,pr_id,product_id,product_type',
      'purchaseItem.purchaseRequisition:id,dpm_no',
      'purchaseItem.product:id,measure_id,item_id,brand_id,category_id,name,code,part_number',
      'purchaseItem.product.measure',
      'purchaseItem.product.item',
      'purchaseItem.product.brand',
      'purchaseItem.product.category',
      'purchaseItem.poItems:id,product_id,pr_item_id,po_id,price',
      'purchaseItem.poItems.po:id,doc_no',
    ])
      ->select([
        'id',
        'spb_id',
        'pr_item_id',
        'no',
        'uuid',
        'bpb_status',
        'qty',
        'qty_parsial',
        'annotation'
      ]);
  }

  public function scopeQueryAllActive() {
    $statusActives = self::$statusActives;
    return $this
      ->scopeQueryDetail()
      ->whereIn('bpb_status', $statusActives);
  }

  public function scopeQueryAllDone() {
    $statusDones = self::$statusDones;
    return $this
      ->scopeQueryDetail()
      ->whereIn('bpb_status', $statusDones);
  }

  public function spb() {
    return $this->belongsTo(SPB::class, 'spb_id');
  }

  public function purchaseItem() {
    return $this->belongsTo(PurchaseItem::class, 'pr_item_id', 'id');
  }

  public function approver() {
    return $this->belongsTo(User::class, 'last_approved', 'id');
  }
}
