<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ReminderCommand extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'emails:send';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Send reminder notice';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle() {
    Mail::send([], [], function ($message) {
      $message->from('sender@gmail.com')
        ->to('reciver@gmail.com')
        ->subject('Pengingat Bray')
        ->setBody('Hi, welcome user!');
    });
    $this->info('The emails are send successfully!');
  }
}
