<?php

namespace App;

class PO extends BaseModel {
  protected $casts = [];
  protected $table = 'po';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }
}
