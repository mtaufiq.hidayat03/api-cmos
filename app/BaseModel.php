<?php

namespace App;

use App\Models\SppdHotels;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
  private $fieldUser = [
    'id', 'name', 'email', 'photo'
  ];
  protected $guarded = [
    'id', 'created_at', 'updated_at'
  ];
  protected $hidden = [
    'pivot'
  ];

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function creator() {
    return $this->belongsTo(User::class, 'created_by')->select($this->fieldUser);
  }

  public function updater() {
    return $this->belongsTo(User::class, 'updated_by')->select($this->fieldUser);
  }
  
  public function hotel() {
    return $this->belongsTo(SppdHotels::class, 'sppd_id')->select(['id','name']);
  }
}