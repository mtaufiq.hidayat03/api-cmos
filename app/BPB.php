<?php

namespace App;

class BPB extends BaseModel {
  protected $casts = [];
  protected $table = 'bpb';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function bpbItems() {
    return $this->hasMany(BPBItem::class, 'bpb_id');
  }
}
