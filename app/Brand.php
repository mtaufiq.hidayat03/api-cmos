<?php

namespace App;

class Brand extends BaseModel {
  protected $casts = [];
  protected $table = 'master_item_brands';

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
