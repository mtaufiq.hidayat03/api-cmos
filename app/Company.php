<?php

namespace App;

class Company extends BaseModel {
  protected $casts = [];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
