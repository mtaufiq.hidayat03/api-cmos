<?php

namespace App\Exports;

use Illuminate\Support\Arr;
use Maatwebsite\Excel\Excel;
use App\InventoryStockOpname;
use Illuminate\Support\Facades\Date;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use App\Spatie\Filters\InventoryStockOpnameFilter;



class InventoryStockOpnameExcel
implements FromQuery, WithHeadings,  ShouldAutoSize, WithStyles, WithEvents, WithMapping, WithCustomStartCell {
  use Exportable;

  private $startDate = '-';
  private $endDate = '-';

  private function populateSheet($sheet) {
    $myStyle = [
      'font' => [
        'bold' => true,
      ],
    ];
    $sheet->setCellValue('B4', "Periode $this->startDate sampai $this->endDate");
    $sheet->getDelegate()->getStyle('B4')->applyFromArray($myStyle);
  }

  private function checkDateFunction($item) {
    $tmp = $item ?? false;
    if ($tmp) {
      $tmp = \Carbon\Carbon::parse($item)->format('d-M-Y');
    } else {
      $tmp = '';
    }
    return $tmp;
  }

  public function startCell(): string {
    return 'B6';
  }

  public function map($item): array {
    return [
      Arr::get($item, 'created_at', '-'),
      '-',
      Arr::get($item, 'inventory.product.code', '-'),
      Arr::get($item, 'inventory.product.name', '-'),
      Arr::get($item, 'inventory.product.measure.name', '-'),
      Arr::get($item, 'inventory.product.item.type', '-'),
      Arr::get($item, 'inventory.location.name', '-'),
      Arr::get($item, 'actual_stock', '-'),
      Arr::get($item, 'good_stock', '-'),
      Arr::get($item, 'bad_stock', '-'),
      Arr::get($item, 'notes', '-'),
      Arr::get($item, 'creator.name', '-'),
    ];
  }

  public function query() {
    $dateFilter = Arr::get(request(), 'filter.between_date');
    $dateFilterArr = null;
    if ($dateFilter) {
      $dateFilterArr = explode(',', $dateFilter);
      $this->startDate = $dateFilterArr[0];
      $this->endDate = $dateFilterArr[1];
    }
    $mapperExactField = function ($item) {
      return AllowedFilter::exact($item);
    };
    $myQuery = InventoryStockOpname::queryListExcel();
    $queryBuilder = QueryBuilder::for($myQuery);
    $columnFiltersExact = [
      'inventory.location_id',
      'created_by'
    ];
    $columnFiltersExact =  array_map($mapperExactField, $columnFiltersExact);
    $columnFilters = [
      AllowedFilter::custom('between_date', new InventoryStockOpnameFilter(), 'created_at')
    ];
    $sortsAdditional = [];
    $filterAdditional = [];
    $filterAdditional = array_merge($filterAdditional, $columnFilters);
    $filterAdditional = array_merge($filterAdditional, $columnFiltersExact);
    $resultQB = $queryBuilder
      ->allowedFilters($filterAdditional)
      ->allowedSorts($sortsAdditional);
    return $resultQB;
  }

  // Custom Fields
  public function headings(): array {
    return [
      'TANGGAL',
      'RAK',
      'CODE',
      'NAMA BARANG / DESKRIPSI',
      'TIPE',
      'STN',
      'LOKASI',
      'AKTUAL STOK',
      'GOOD STOK',
      'BAD STOK',
      'NOTE',
      'DIINPUT OLEH',
    ];
  }

  // Custom Styles
  public function styles(Worksheet $sheet) {
    $maxCol = $sheet->getHighestColumn();
    $maxRow = $sheet->getHighestRow();
    $first = 'B1';
    $last = $maxCol . $maxRow;
    $sheet->getStyle("$first:$last")->getAlignment()->setHorizontal('left');
    return [
      // Style the first row as bold text.
      6 => [
        'font' => [
          'bold' => true,
          'color' => ['rgb' => 'ffffff']
        ],
      ],
    ];
  }

  public function registerEvents(): array {
    return [
      AfterSheet::class => function (AfterSheet $event) {
        $sheet = $event->sheet;
        $this->populateSheet($sheet);
        $myStyle = [
          'font' => [
            'bold' => true,
          ],
        ];
        $cellRange = 'B6:M6'; // All headers
        $delegate = $event->sheet->getDelegate();
        $headerRows = $delegate->getStyle($cellRange);
        $headerRows->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()
          ->setRGB('3366FF');
        $paragraph2 = $delegate->setCellValue('B3', "                        Export Data Inventory Stock Opname")
          ->mergeCells('B3:M3');
        $paragraph2->getStyle('B3:M3')->applyFromArray($myStyle);
        // $paragraph3 = $delegate->setCellValue('B4', "")
        //   ->mergeCells('B4:M3');
        // $paragraph3->getStyle('B4:M3')->applyFromArray($myStyle);
      },
    ];
  }
}
