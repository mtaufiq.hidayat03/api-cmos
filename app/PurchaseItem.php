<?php

namespace App;

class PurchaseItem extends BaseModel {
  protected $casts = [];
  protected $table = 'purchase_items';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function purchaseRequisition() {
    return $this->belongsTo(PurchaseRequisition::class, 'pr_id', 'id');
  }

  public function product() {
    return $this->belongsTo(Product::class, 'product_id', 'id');
  }

  public function poItems() {
    return $this->hasOne(POItem::class, 'pr_item_id', 'id');
  }
}
