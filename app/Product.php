<?php

namespace App;

use App\Location;

class Product extends BaseModel {
  protected $casts = [
    'status' => 'boolean',
  ];
  protected $table = 'master_item_products';
  public static $showAttributes = [
    'id',
    'name',
    'code',
    'part_number',
    'description',
    'satuan',
    'created_at',
    'updated_at',
    'created_by',
    'updated_by',
    'brand_id',
    'item_id',
    'category_id',
    'measure_id',
  ];

  public function scopeQueryAll() {
    return $this->with(['creator', 'category', 'measure', 'item'])->select(self::$showAttributes);
  }

  public function category() {
    return $this->belongsTo(Category::class)->select(['id', 'name', 'pid']);
  }

  public function measure() {
    return $this->belongsTo(Measure::class)->select(['id', 'name']);
  }

  public function item() {
    return $this->belongsTo(Item::class)->select(['id', 'name', 'type', 'code']);
  }

  public function brand() {
    return $this->belongsTo(Brand::class)->select(['id', 'name']);
  }
}
