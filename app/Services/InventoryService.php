<?php

namespace App\Services;

use Illuminate\Support\Arr;
use App\Repositories\InventoryRepository;
use App\Repositories\InventoryHistoryRepository;

class InventoryService extends BaseService {
  protected $inventoryRepo;
  protected $inventoryHistoryRepo;

  public function __construct(
    InventoryRepository $inventoryRepo,
    InventoryHistoryRepository $inventoryHistoryRepo
  ) {
    $this->inventoryRepo = $inventoryRepo;
    $this->inventoryHistoryRepo = $inventoryHistoryRepo;
  }

  private function saveInventoryHistory($value) {
    $this->inventoryHistoryRepo->create($value);
  }

  public function getAll() {
    $search = request()->search;
    $sortAndFilters = ['location_id', 'stock_min', 'stock_max', 'stock_onhand', 'in', 'out', 'initial', 'product_type', 'uuid'];
    $columnSorts = $sortAndFilters;
    $columnFilters = $sortAndFilters;
    return $this->showResponsePaginate($this->inventoryRepo->queryGetAll($search), $columnSorts, $columnFilters);
  }

  public function get($id) {
    $inventory = $this->inventoryRepo->get($id);
    if (!$inventory) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventory);
  }

  public function qrCode($uuid) {
    $inventory = $this->inventoryRepo->getWithQr($uuid);
    if (!$inventory) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventory);
  }

  public function dirtyStore($item) {
    // Logic inventory, jika ada
    $productId = Arr::get($item, 'product_id');
    $locationId = Arr::get($item, 'location_id');
    $qtyInput = Arr::get($item, 'in');
    $bpbDocNumber = Arr::get($item, 'bpb_doc_number');
    $noPo = Arr::get($item, 'no_po');
    $price = Arr::get($item, 'price');
    $data = null;
    $prevInventory = $this->inventoryRepo->getByProductAndLocation($productId,$locationId);
    if ($prevInventory) {
      $item['in'] = $prevInventory->in + $qtyInput;
      $item['stock_onhand'] = $prevInventory->stock_onhand + $qtyInput;
      $max = $prevInventory->stock_max;
      $min = $prevInventory->stock_min;
      $onHand = $prevInventory->stock_onhand;
      $item['stock_status'] = $this->inventoryRepo->getStatusInventory($max, $min, $onHand);
      $prevInventory->fill($item);
      $prevInventory->save();
      $itemInventoryHistory['inventory_id'] = $prevInventory->id;
      $itemInventoryHistory['qty_in'] = $qtyInput;
      $itemInventoryHistory['message'] = $bpbDocNumber;
      $itemInventoryHistory['notes'] = $this->inventoryHistoryRepo->setNotes($noPo, $price);
      $itemInventoryHistory['qty_awal'] = $onHand;
      $this->saveInventoryHistory($itemInventoryHistory);
      $data = $prevInventory;
    } else {
      $data = $this->inventoryRepo->create($item);
      $itemInventoryHistory['inventory_id'] = $data->id;
      $itemInventoryHistory['qty_in'] = $qtyInput;
      $itemInventoryHistory['message'] = $bpbDocNumber;
      $itemInventoryHistory['notes'] = $this->inventoryHistoryRepo->setNotes($noPo, $price);
      $itemInventoryHistory['qty_awal'] = 0;
      $this->saveInventoryHistory($itemInventoryHistory);
    }
    return $data;
  }

  public function update($id) {
    $values = request()->all();
    $inventory = $this->inventoryRepo->update($id, $values);
    if (!$inventory) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventory);
  }

  public function destroy($id) {
    $inventory = $this->inventoryRepo->destroy($id);
    if (!$inventory) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventory);
  }
}
