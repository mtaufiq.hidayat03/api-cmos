<?php

namespace App\Services;

use Illuminate\Support\Str;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ForgotPasswordService extends BaseService {
  protected $userRepo;

  public function __construct(UserRepository $userRepo) {
    $this->userRepo = $userRepo;
  }

  public function forgotPassword() {
    $values = request()->all();
    $email = $values['email'];
    $status = Password::sendResetLink(['email' => $email], function (Message $message) {
      $message->subject($this->getEmailSubject());
    });
    switch ($status) {
      case Password::RESET_LINK_SENT:
        return $this->showResponse('a reset link has been sent to your email address.');
      case Password::INVALID_USER:
        return $this->showResponse('invalid email', true);
      default:
        return $this->showResponse('reset link has been already sended into your email, please check !', true);
    }
  }

  public function updatePassword() {
    $values = request()->all();
    $credentials['email'] = $values['email'];
    $credentials['password'] = $values['password'];
    $credentials['token'] = $values['token'];
    $status = Password::reset(
      $credentials,
      function ($user, $password) {
        $user->forceFill([
          'password' => Hash::make($password)
        ])->save();
        $user->setRememberToken(Str::random(60));
      }
    );
    if ($status == Password::PASSWORD_RESET) {
      return $this->showResponse('success reset password, please login with use new password !');
    } else {
      return $this->showResponse('reset token has expired / not match', true);
    }
  }
}
