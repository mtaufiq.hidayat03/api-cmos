<?php

namespace App\Services;

use App\Exports\InventoryStockOpnameExcel;
use App\Repositories\InventoryRepository;
use App\Repositories\InventoryStockOpnameRepository;
use Maatwebsite\Excel\Facades\Excel;

class InventoryStockOpnameService extends BaseService {
  protected $inventoryStockOpnameRepo;
  protected $inventoryRepository;

  private $columnSortFilters = ['actual_stock', 'good_stock', 'bad_stock', 'notes'];
  private $columnExactFilters = ['inventory_id'];

  public function __construct(
    InventoryStockOpnameRepository $inventoryStockOpnameRepo,
    InventoryRepository $inventoryRepository
  ) {
    $this->inventoryStockOpnameRepo = $inventoryStockOpnameRepo;
    $this->inventoryRepository = $inventoryRepository;
  }

  public function getAll() {
    $search = request()->search;
    $columnSortFilters = $this->columnSortFilters;
    $columnExactFilters = $this->columnExactFilters;
    return $this->showResponsePaginate($this->inventoryStockOpnameRepo->queryGetAll($search), $columnSortFilters, $columnSortFilters, $columnExactFilters);
  }

  public function get($id) {
    $inventoryStockOpname = $this->inventoryStockOpnameRepo->get($id);
    if (!$inventoryStockOpname) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventoryStockOpname);
  }

  public function qrCode($uuid) {
    $inventory = $this->inventoryRepository->getWithQr($uuid);
    $columnSortFilters = $this->columnSortFilters;
    $columnExactFilters = $this->columnExactFilters;
    if (!$inventory) {
      return $this->showResponse('data not found', true);
    }
    $inventoryStockOpname = $this->inventoryStockOpnameRepo->queryGetByInventoryId($inventory->id);
    return $this->showResponsePaginate($inventoryStockOpname, $columnSortFilters, $columnSortFilters, $columnExactFilters);
  }

  public function create() {
    $values = request()->all();
    $inventoryStockOpname = $this->inventoryStockOpnameRepo->create($values);
    return $this->showResponse($inventoryStockOpname);
  }

  public function update($id) {
    $values = request()->all();
    $inventoryStockOpname = $this->inventoryStockOpnameRepo->update($id, $values);
    if (!$inventoryStockOpname) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventoryStockOpname);
  }

  public function destroy($id) {
    $inventoryStockOpname = $this->inventoryStockOpnameRepo->destroy($id);
    if (!$inventoryStockOpname) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($inventoryStockOpname);
  }

  public function exportExcel() {
    return Excel::download(new InventoryStockOpnameExcel(), 'InventoryStockOpname.xlsx');
  }
}
