<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\User;

class UserService extends BaseService {
  protected $userRepo;

  public function __construct(UserRepository $userRepo) {
    $this->userRepo = $userRepo;
  }

  public function getAll() {
    return $this->showResponsePaginate($this->userRepo->queryGetAll());
  }

  public function get($id) {
    $user = $this->userRepo->get($id);
    if (!$user) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($user);
  }

  public function update($id) {
    $values = request()->all();
    $user = $this->userRepo->update($id, $values);
    if (!$user) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($user);
  }

  public function destroy($id) {
    $user = $this->userRepo->destroy($id);
    $isCurrentUser = $this->userRepo->isCurrent($id);
    if ($isCurrentUser) return $this->showResponse('cannot delete current data', true);
    if (!$user) return $this->showResponse('data not found', true);
    return $this->showResponse($user);
  }
}
