<?php

namespace App\Services;

use Illuminate\Support\Collection;
use App\Repositories\BPBRepository;
use App\Repositories\BPBItemRepository;
use App\Repositories\SPBKolisRepository;
use App\Repositories\SPBRepository;
use Illuminate\Support\Arr;

class BPBItemService extends BaseService {
  protected $spbKolisRepository;
  protected $spbRepository;
  protected $bpbItemRepository;
  protected $inventoryService;

  public function __construct(
    SPBKolisRepository $spbKolisRepository,
    SPBRepository $spbRepository,
    BPBItemRepository $bpbItemRepository,
    InventoryService $inventoryService
  ) {
    $this->spbKolisRepository = $spbKolisRepository;
    $this->spbRepository = $spbRepository;
    $this->bpbItemRepository = $bpbItemRepository;
    $this->inventoryService = $inventoryService;
  }

  private function storeInventory($item){
    return $this->inventoryService->dirtyStore($item);
  }

  private function actualStore($item) {
    $this->bpbItemRepository->create($item);
    $spbItemId = Arr::get($item,'spb_item_id');
    $qtyInput = Arr::get($item,'qty');
    $inventoryValue['product_id'] = Arr::get($item,'product_id');
    $inventoryValue['product_type'] = Arr::get($item,'product_type');
    $inventoryValue['location_id'] = Arr::get($item,'location_id');
    $inventoryValue['stock_onhand'] = $qtyInput;
    $inventoryValue['in'] = $qtyInput;
    $inventoryValue['initial'] = 0;
    $inventoryValue['price'] = (int) Arr::get($item,'price');
    $inventoryValue['bpb_doc_number'] = Arr::get($item,'bpb_doc_number');
    $inventoryValue['no_po'] = Arr::get($item,'no_po');
    $dataInven = $this->storeInventory($inventoryValue);
    $this->spbKolisRepository->setBpbStatusByQtyIn($spbItemId, $qtyInput);
    $this->spbRepository->setStatusWithSpbKolis($item['spb_id']);
    return $dataInven;
  }

  public function dirtyStore($item) {
    $spbKolisId = Arr::get($item, 'spb_kolis_id');
    $spbKolis = $this->spbKolisRepository->get($spbKolisId);
    if(!$spbKolis) return false;
    $item['pr_item_id'] = $spbKolis->pr_item_id;
    $item['spb_item_id'] = $spbKolis->id;
    $item['bpb_id'] = Arr::get($item, 'bpb_id');
    return $this->actualStore($item);
  }
}
