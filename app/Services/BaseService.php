<?php

namespace App\Services;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Schema;
use Spatie\QueryBuilder\AllowedFilter;

class BaseService {
  private function setQueryBuilder($modelClass,  $columnSorts, $columnFilters, $columnFiltersExact) {
    $mapperExactField = function ($item) {
      return AllowedFilter::exact($item);
    };
    $paginate = request('limit') ?? 25;
    $columnSortsMust = ['id', 'created_at'];
    $columnSorts = array_merge($columnSortsMust, $columnSorts);
    $columnFiltersExactMust = ['id', 'created_by'];
    $columnFiltersExact = array_merge($columnFiltersExactMust, $columnFiltersExact);
    $columnFiltersExact =  array_map($mapperExactField, $columnFiltersExact);
    // Join All Filter
    $columnFilters = array_merge($columnFilters, $columnFiltersExact);
    $queryBuilder = QueryBuilder::for($modelClass);
    $resultQB = $queryBuilder
      ->allowedSorts($columnSorts)
      ->allowedFilters($columnFilters)
      ->paginate($paginate)
      ->appends(request()->query());
    return $resultQB;
  }

  protected function showResponse($data, $isFail = false) {
    $status = $isFail ? 'error' : 'success';
    $code = $isFail ? 400 : 200;
    return response()->json([
      'status' => $status,
      'data' => $data,
    ], $code);
  }

  protected function showResponseNotFound() {
    return $this->showResponse('data not found', true);
  }

  protected function showResponseUnauth() {
    return response()->json([
      'status' => 'error',
      'data' => 'not authorize permissions',
    ], 401);
  }

  protected function showResponsePaginate($myQuery,  $columnSorts = [], $columnFilters = [], $columnFiltersExact = []) {
    $resultQueryBuilder = $this->setQueryBuilder($myQuery, $columnSorts, $columnFilters, $columnFiltersExact);
    $res = [
      'status' => 'success',
      'data' => $resultQueryBuilder->items(),
      'pagination' => [
        'current_page' => (int) $resultQueryBuilder->currentPage(),
        'total' => (int) $resultQueryBuilder->total(),
        'per_page' => (int) $resultQueryBuilder->perPage(),
        'last_page' => (int) $resultQueryBuilder->lastPage(),
      ],
    ];
    return $res;
  }

  protected function showReponseDownload($fileName) {
    $folderPath = public_path() . "/files/";
    $file = $folderPath . $fileName;
    $headers = array(
      'Content-Type: ' . mime_content_type($file),
    );
    if (!$file) return $this->showResponse('data not found', true);
    return response()->download($file, $fileName, $headers);
  }
}
