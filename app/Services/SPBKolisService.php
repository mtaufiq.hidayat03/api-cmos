<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Repositories\SPBKolisRepository;
use App\Repositories\BPBRepository;
use App\Repositories\BPBItemRepository;
use DB;
use Exception;

class SPBKolisService extends BaseService {
  protected $spbKolisRepo;
  protected $bpbRepository;
  protected $bpbItemService;

  public function __construct(
    SPBKolisRepository $spbKolisRepo,
    BPBRepository $bpbRepository,
    BPBItemService $bpbItemService
  ) {
    $this->spbKolisRepo = $spbKolisRepo;
    $this->bpbRepository = $bpbRepository;
    $this->bpbItemService = $bpbItemService;
  }

  public function qrCode($uuid) {
    $spbKolis = $this->spbKolisRepo->getWithQr($uuid);
    if (!$spbKolis) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($spbKolis);
  }

  public function store() {
    $bpbReq = request()->all();
    $bpbItemsReq = request()->spb_items;
    unset($bpbReq['spb_items']);
    try {
      DB::beginTransaction();
      $bpb = $this->bpbRepository->create($bpbReq);
      $bpbItemService = $this->bpbItemService;
      $test = null;
      $bpbItemsReq = array_map(function ($item) use ($bpbItemService, $bpbReq, &$bpb, &$test) {
        $item['location_id'] = Arr::get($bpbReq, 'location_id');
        $item['spb_id'] = Arr::get($bpbReq, 'spb_id');
        $item['bpb_doc_number'] = $bpb->doc_no;
        $item['bpb_id'] = $bpb->id;
        $test = $bpbItemService->dirtyStore($item);
        return $item;
      }, $bpbItemsReq);
      DB::commit();
      $res = $bpb->load(['bpbItems']);
      return $this->showResponse($res);
    } catch (Exception $e) {
      lad($e);
      DB::rollBack();
      return $this->showResponse('failed create data', true);
    }
  }
}
