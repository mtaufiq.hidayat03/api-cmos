<?php


namespace App\Services\Hr;

use App\Models\ECuti;
use App\Repositories\Hr\SppdRepository;
use App\Services\BaseService;
use Carbon\Carbon;

class SppdService extends BaseService
{
  protected $sppdRepo;

  public function __construct(SppdRepository $sppdRepo) {
    $this->sppdRepo = $sppdRepo;
  }

  public function getAllSppd() {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->sppdRepo->queryGetAll(), $columnSorts, $columnFilters);
  }

  public function getAllSppdByUser($userId) {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->sppdRepo->queryGetAllByUser($userId), $columnSorts, $columnFilters);
    // return $userId;
  }

  public function getSppdById($sppdId){
    return $this->showResponse($this->sppdRepo->getSppdById($sppdId));
  }

  public function create($status){
    $values = request()->all();
    $checkApproval = $this->sppdRepo->checkApproval();
    if($checkApproval){
      return $this->showResponse($this->sppdRepo->createSppd($values, $status));
    } else {
      return $this->showResponse('Pengajuan tidak berhasil dikarenakan Rule Approval Anda belum disetting. Silahkan hubungi bagian HRD.!',true);
    }
  }

  public function createAdvance($sppdId){
      $values = request()->all();
      return $this->showResponse($this->sppdRepo->createSppdAdvance($values, $sppdId));
  }

  public function createHotel($sppdId){
    $values = request()->all();
    return $this->showResponse($this->sppdRepo->createSppdHotel($values, $sppdId));
}

public function createFlight($sppdId){
  $values = request()->all();
  return $this->showResponse($this->sppdRepo->createSppdFlight($values, $sppdId));
}

  
  public function destroy($id) {
    $sppd = $this->sppdRepo->destroy($id);
    if (!$sppd) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($sppd);
  }

  public function updateSppd($sppdId, $status, $type){
    $values = request()->all(); 
    $sppd = $this->sppdRepo->updateSppd($sppdId, $status, $values, $type);
    if($sppd == false) return $this->showResponse('data not found', true);
    return $this->showResponse($sppd);
   
  }

  //Ticket
  public function getAllSppdTicket($id) {
    return $this->showResponsePaginate($this->sppdRepo->queryGetAllTicket($id));
  }

  public function sppdTicketDestroy($id){
    $ticket = $this->sppdRepo->sppdTicketDestroy($id);
    if (!$ticket) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($ticket);
  }

  public function sppdTicketUpdate($id){
    $values = request()->all(); 
    $sppdFlight = $this->sppdRepo->sppdTicketUpdate($values, $id);
    if($sppdFlight == false) return $this->showResponse('data not found', true);
    return $this->showResponse($sppdFlight);
   
  }

  //Hotel
  public function getAllSppdHotel($id) {
    return $this->showResponsePaginate($this->sppdRepo->queryGetAllHotel($id));
  }
  public function sppdHotelDestroy($id){
    $hotel = $this->sppdRepo->sppdHotelDestroy($id);
    if (!$hotel) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($hotel);
  }

  public function sppdHotelUpdate($id){
    $values = request()->all(); 
    $sppdHotel = $this->sppdRepo->sppdHotelUpdate($values, $id);
    if($sppdHotel == false) return $this->showResponse('data not found', true);
    return $this->showResponse($sppdHotel);
   
  }

  //Advances
  public function getAllSppdAdvances($id) {
    return $this->showResponsePaginate($this->sppdRepo->queryGetAllAdvances($id));
  }

  public function sppdAdvanceDestroy($id){
    $advance = $this->sppdRepo->sppdAdvanceDestroy($id);
    if (!$advance) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($advance);
  }
  
  public function sppdAdvancesUpdate($id){
    $values = request()->all(); 
    $sppdAdvances = $this->sppdRepo->sppdAdvancesUpdate($values, $id);
    if($sppdAdvances == false) return $this->showResponse('data not found', true);
    return $this->showResponse($sppdAdvances);
   
  }
  
  public function downloadSppdPdf($id){
    return $this->sppdRepo->downloadSppdPdf($id);
  }

}