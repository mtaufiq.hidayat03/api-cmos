<?php


namespace App\Services\Hr;

use App\Repositories\Hr\CityRepository;
use App\Services\BaseService;

class CityService extends BaseService
{
  protected $cityRepo;

  public function __construct(CityRepository $cityRepo) {
    $this->cityRepo = $cityRepo;
  }

  public function getCity(){
    return $this->showResponse($this->cityRepo->queryGetCity());
  }
}
