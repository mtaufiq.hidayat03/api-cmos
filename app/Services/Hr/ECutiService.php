<?php


namespace App\Services\Hr;

use App\Models\ECuti;
use App\Repositories\Hr\ECutiRepository;
use App\Services\BaseService;
use Carbon\Carbon;

class ECutiService extends BaseService
{
  protected $ecutiRepo;

  public function __construct(ECutiRepository $ecutiRepo) {
    $this->ecutiRepo = $ecutiRepo;
  }

  public function getAllCuti() {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ecutiRepo->queryGetAll(), $columnSorts, $columnFilters);
  }

  public function getAllCutiByUser($userId) {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ecutiRepo->queryGetAllByUser($userId), $columnSorts, $columnFilters);
    // return $userId;
  }

  public function get($id) {
    $ecuti = $this->ecutiRepo->get($id);
    if (!$ecuti) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($ecuti);
    // return $this->showResponse($id);
  }

  public function update($id, $status) {
    $values = request()->all();

    $employee = $this->ecutiRepo->checkEmployee();
    $checkLeaveBalance = $this->ecutiRepo->leaveBalance();

    if($checkLeaveBalance && $employee){
      $checkApproval = $this->ecutiRepo->checkApproval();
        if($checkApproval){
            $from = new Carbon ($values['from']);
            $to = new Carbon ($values['to']);

            if($to < $from){
              return $this->showResponse("Tanggal periode berakhir harus melewati tanggal mulai", true);
            }

            $count_days = ($to->diffInDays($from))+1;

            //Weekend Check
            $minusDay = 0;
            $format_date_from = date_create($from);
            $format_date_to = date_create($to);

            $start = date_format($format_date_from, "Y/m/d");
            $end = date_format($format_date_to, "Y/m/d");

            for($i = $start; $i <= $end; $i++){
                if((date('N', strtotime($i)) >= 6) == false){
                    $minusDay += 0;
                } else {
                    $minusDay += 1;
                }
            }

            // Minus Weekend
            $total_days = $count_days - $minusDay;

            $getFirstApproval = $this->ecutiRepo->getApproval(1)[0]->approval_id;

            if($values['leave_type_id'] == 1){
              if($employee->start_date == NULL || $employee->start_date == "0001-01-01"){
                return $this->showResponse("Tanggal mulai bekerja belum di setting, silahkan hubungi bagian HRD. Terima kasih.", true);

              } else if (($employee->start_date != NULL || $employee->start_date != "0001-01-01") && (compareLastThanOneYear($employee->start_date) == TRUE)){
                return $this->showResponse("Masa kerja belum satu tahun, silahkan hubungi bagian HRD. Terima kasih.", true);

              } else if ($employee->start_date != NULL && $employee->start_date != "0001-01-01" && $checkLeaveBalance->leave_start_date != NULL && $checkLeaveBalance->leave_start_date != "0001-01-01" && compareLastThanOneYear($employee->start_date) == FALSE){
                $startDate = Carbon::parse($employee->start_date);
                $leaveStartDate = Carbon::parse($checkLeaveBalance->leave_start_date);
                $diffYear = $startDate->diffInYears($leaveStartDate);

                if($diffYear >= 1 && $checkLeaveBalance->expired_date != NULL) {
                  $newDateExp = Carbon::parse($checkLeaveBalance->expired_date)->subYear(1)->format('Y-m-d');

                  //Saldo cuti - approved cuti - cuti bersama - approved outstanding leave
                  $count = $checkLeaveBalance->balance_count - countApprovedLeaveBalance($checkLeaveBalance->user_id, $checkLeaveBalance->expired_date, 1)[0]->count - countMassLeaveByYearAfterInOffice($employee->start_date) - countLastYearOutstandingLeave($checkLeaveBalance->user_id, 4, $newDateExp, 1)[0]->sum;
                  if($count <= 0){
                    return $this->showResponse("Saldo cuti tahunan anda telah habis, silahkan hubungi bagian HRD. Terima kasih.", true);
                  } else {
                    if($count < $total_days){
                      return $this->showResponse("Saldo cuti tahunan anda tidak cukup, silahkan hubungi bagian HRD. Terima kasih.", true);
                    } else {
                      $values['outstanding_leave'] = false;
                      $eCuti = $this->ecutiRepo->update($id, $values, $status);
                      return $this->showResponse($eCuti);
                    }
                  }
                }
              }
              return $this->showResponse('Something went wrong', true);

            } else {
              $values['outstanding_leave'] = false;
                //draft = 0, release = 1
              $eCuti = $this->ecutiRepo->update($id, $values, $status);
              return $this->showResponse($eCuti);
            }
        } else {
          return $this->showResponse("Rule approval Cuti tidak ditemukan, silahkan hubungi bagian HRD/IT Team. Terima kasih.", true);
        }

    } else {
    return $this->showResponse("Data pegawai tidak ditemukan, silahkan hubungi bagian HRD/IT Team. Terima kasih..", true);
    }

  }

  public function destroy($id) {
    $ecuti = $this->ecutiRepo->destroy($id);
    if (!$ecuti) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($ecuti);
  }

  public function getLeaveTypes(){
    return $this->showResponse($this->ecutiRepo->queryGetLeaveTypes());
  }

  public function create($status){
    $values = request()->all();
    
    $employee = $this->ecutiRepo->checkEmployee();
    $checkLeaveBalance = $this->ecutiRepo->leaveBalance();

    if($checkLeaveBalance && $employee){
      $checkApproval = $this->ecutiRepo->checkApproval();
        if($checkApproval){
            $from = new Carbon ($values['from']);
            $to = new Carbon ($values['to']);

            if($to < $from){
              return $this->showResponse("Tanggal periode berakhir harus melewati tanggal mulai", true);
            }

            $count_days = ($to->diffInDays($from))+1;

            //Weekend Check
            $minusDay = 0;
            $format_date_from = date_create($from);
            $format_date_to = date_create($to);

            $start = date_format($format_date_from, "Y/m/d");
            $end = date_format($format_date_to, "Y/m/d");

            for($i = $start; $i <= $end; $i++){
                if((date('N', strtotime($i)) >= 6) == false){
                    $minusDay += 0;
                } else {
                    $minusDay += 1;
                }
            }

            // Minus Weekend
            $total_days = $count_days - $minusDay;

            $getFirstApproval = $this->ecutiRepo->getApproval(1)[0]->approval_id;

            if($values['leave_type_id'] == 1){
              if($employee->start_date == NULL || $employee->start_date == "0001-01-01"){
                return $this->showResponse("Tanggal mulai bekerja belum di setting, silahkan hubungi bagian HRD. Terima kasih.", true);

              } else if (($employee->start_date != NULL || $employee->start_date != "0001-01-01") && (compareLastThanOneYear($employee->start_date) == TRUE)){
                return $this->showResponse("Masa kerja belum satu tahun, silahkan hubungi bagian HRD. Terima kasih.", true);

              } else if ($employee->start_date != NULL && $employee->start_date != "0001-01-01" && $checkLeaveBalance->leave_start_date != NULL && $checkLeaveBalance->leave_start_date != "0001-01-01" && compareLastThanOneYear($employee->start_date) == FALSE){
                $startDate = Carbon::parse($employee->start_date);
                $leaveStartDate = Carbon::parse($checkLeaveBalance->leave_start_date);
                $diffYear = $startDate->diffInYears($leaveStartDate);

                if($diffYear >= 1 && $checkLeaveBalance->expired_date != NULL) {
                  $newDateExp = Carbon::parse($checkLeaveBalance->expired_date)->subYear(1)->format('Y-m-d');

                  //Saldo cuti - approved cuti - cuti bersama - approved outstanding leave
                  $count = $checkLeaveBalance->balance_count - countApprovedLeaveBalance($checkLeaveBalance->user_id, $checkLeaveBalance->expired_date, 1)[0]->count - countMassLeaveByYearAfterInOffice($employee->start_date) - countLastYearOutstandingLeave($checkLeaveBalance->user_id, 4, $newDateExp, 1)[0]->sum;
                  if($count <= 0){
                    return $this->showResponse("Saldo cuti tahunan anda telah habis, silahkan hubungi bagian HRD. Terima kasih.", true);
                  } else {
                    if($count < $total_days){
                      return $this->showResponse("Saldo cuti tahunan anda tidak cukup, silahkan hubungi bagian HRD. Terima kasih.", true);
                    } else {
                      
                      $values['outstanding_leave'] = false;
                      $eCuti = $this->ecutiRepo->create($values, $status, $getFirstApproval);
                      return $this->showResponse($eCuti);
                    }
                  }
                }
              }
              return $this->showResponse('Something went wrong', true);

            } else {
              
              $values['outstanding_leave'] = false;
                //draft = 0, release = 1
              $eCuti = $this->ecutiRepo->create($values, $status, $getFirstApproval);
              return $this->showResponse($eCuti);
            }
        } else {
          return $this->showResponse("Rule approval Cuti tidak ditemukan, silahkan hubungi bagian HRD/IT Team. Terima kasih.", true);
        }

    } else {
    return $this->showResponse("Data pegawai tidak ditemukan, silahkan hubungi bagian HRD/IT Team. Terima kasih..", true);
    }
  }

  public function getLeaveBalance(){
    return $this->showResponse($this->ecutiRepo->getLeaveBalance());
  }
}
