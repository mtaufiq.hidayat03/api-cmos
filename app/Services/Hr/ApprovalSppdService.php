<?php


namespace App\Services\Hr;

use App\Models\ECuti;
use App\Repositories\Hr\ApprovalSppdRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ApprovalSppdService extends BaseService
{
  protected $ApprovalSppdRepo;

  public function __construct(ApprovalSppdRepository $ApprovalSppdRepo) {
    $this->ApprovalSppdRepo = $ApprovalSppdRepo;
  }

  public function getAllSppd() {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ApprovalSppdRepo->queryGetAll(), $columnSorts, $columnFilters);
  }

  public function getAllSppdByApprover($userId) {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ApprovalSppdRepo->queryGetAllByApprover($userId), $columnSorts, $columnFilters);
    // return $userId;
  }

  public function get($id) {
    $sppd = $this->ApprovalSppdRepo->get($id);
    if (!$sppd) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($sppd);
    // return $this->showResponse($id);
  }

  public function approveAction($id, $status, $user_id) {
    $user = Auth::user();
    $getTotalApproval = $this->ApprovalSppdRepo->getSelectedApproval($user_id);
    $getStepApproval = $this->ApprovalSppdRepo->getStepApprovalSppd($user_id)[0]->step;
   
    // // return $getStepApproval.$getTotalApproval;
    if($getStepApproval < $getTotalApproval){
      if($status == 1){
        $getNextApproval = $this->ApprovalSppdRepo->getApprovalSppd($user_id, $getStepApproval+1)[0]->approval_id;
        // return $getNextApproval;
        $sppd = $this->ApprovalSppdRepo->approveAction($id, 2, $getNextApproval, $user_id);
        if (!$sppd) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($sppd);
        // return $sppd;
      } else {
        $sppd = $this->ApprovalSppdRepo->approveAction($id, 3, $user->id, $user_id);
        if (!$sppd) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($sppd);
      }

    } else if ($getStepApproval == $getTotalApproval){
      if($status == 1){
        $sppd = $this->ApprovalSppdRepo->approveAction($id, 4, $user->id, $user_id);
        if (!$sppd) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($sppd);
      } else {
        $sppd = $this->ApprovalSppdRepo->approveAction($id, 3, $user->id, $user_id);
        if (!$sppd) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($sppd);
      }
    }
    return $this->showResponse('Data not found', true);
  }


}