<?php


namespace App\Services\Hr;

use App\Models\ECuti;
use App\Repositories\Hr\ApprovalECutiRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ApprovalECutiService extends BaseService
{
  protected $ApprovalECutiRepo;

  public function __construct(ApprovalECutiRepository $ApprovalECutiRepo) {
    $this->ApprovalECutiRepo = $ApprovalECutiRepo;
  }

  public function getAllCuti() {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ApprovalECutiRepo->queryGetAll(), $columnSorts, $columnFilters);
  }

  public function getAllCutiByApprover($userId) {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->ApprovalECutiRepo->queryGetAllByApprover($userId), $columnSorts, $columnFilters);
    // return $userId;
  }

  public function get($id) {
    $ecuti = $this->ApprovalECutiRepo->get($id);
    if (!$ecuti) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($ecuti);
    // return $this->showResponse($id);
  }

  public function approveAction($id, $status, $user_id) {
    $user = Auth::user();
    $getTotalApproval = $this->ApprovalECutiRepo->getSelectedApproval($user_id);
    $getStepApproval = $this->ApprovalECutiRepo->getStepApprovalEcuti($user_id)[0]->step;
   
    // // return $getStepApproval.$getTotalApproval;
    if($getStepApproval < $getTotalApproval){
      if($status == 1){
        $getNextApproval = $this->ApprovalECutiRepo->getApprovalEcuti($user_id, $getStepApproval+1)[0]->approval_id;
        // return $getNextApproval;
        $ecuti = $this->ApprovalECutiRepo->approveAction($id, 2, $getNextApproval);
        if (!$ecuti) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($ecuti);
        // return $ecuti;
      } else {
        $ecuti = $this->ApprovalECutiRepo->approveAction($id, 3, $user->id);
        if (!$ecuti) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($ecuti);
      }

    } else if ($getStepApproval == $getTotalApproval){
      if($status == 1){
        $ecuti = $this->ApprovalECutiRepo->approveAction($id, 4, $user->id);
        if (!$ecuti) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($ecuti);
      } else {
        $ecuti = $this->ApprovalECutiRepo->approveAction($id, 3, $user->id);
        if (!$ecuti) {
          return $this->showResponse('data not found', true);
        }
        return $this->showResponse($ecuti);
      }
    }
    return $this->showResponse('Data not found', true);
  }


}