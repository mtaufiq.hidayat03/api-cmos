<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Repositories\LocationRepository;

class LocationService extends BaseService {
  protected $locationRepo;

  public function __construct(LocationRepository $locationRepo) {
    $this->locationRepo = $locationRepo;
  }

  public function getAll() {
    $search = request()->search;
    $sortAndFilters = [];
    $columnSorts = $sortAndFilters;
    $columnFilters = $sortAndFilters;
    return $this->showResponsePaginate($this->locationRepo->queryAll($search), $columnSorts, $columnFilters);
  }

  public function getAllDropdown() {
    $datas = $this->locationRepo->queryAllDropdown();
    $collectionItems = new Collection();
    foreach ($datas as $item) {
      $collectionItems->push((object) $item);
    }
    $collectionItems->map(function ($item) {
      $companyName = Arr::get($item,'company.name');
      $item->name = "$item->name [$companyName]";
      return $item;
    });
    return $this->showResponse($collectionItems);
  }

  public function get($id) {
    $location = $this->locationRepo->get($id);
    if (!$location) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($location);
  }

  public function qrCode($uuid) {
    $location = $this->locationRepo->getWithQr($uuid);
    if (!$location) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($location);
  }

  public function update($id) {
    $values = request()->all();
    $location = $this->locationRepo->update($id, $values);
    if (!$location) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($location);
  }

  public function destroy($id) {
    $location = $this->locationRepo->destroy($id);
    if (!$location) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($location);
  }
}
