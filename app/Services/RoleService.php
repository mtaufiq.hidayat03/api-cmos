<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

class RoleService extends BaseService {
  protected $roleRepo;

  public function __construct(RoleRepository $roleRepo) {
    $this->roleRepo = $roleRepo;
  }

  public function index() {
    return $this->showResponsePaginate($this->roleRepo->queryGetAll());
  }

  public function show($id) {
    $role = $this->roleRepo->get($id);
    if (!$role) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($role);
  }
}
