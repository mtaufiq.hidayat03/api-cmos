<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

class AuthService extends BaseService {
  protected $userRepo;

  public function __construct(UserRepository $userRepo) {
    $this->userRepo = $userRepo;
  }

  private function getCookieDetails($token) {
    return [
      'name' => '_token',
      'value' => $token,
      'minutes' => 1440,
      'path' => null,
      'domain' => null,
      // 'secure' => true, // for production
      'secure' => false, // for localhost
      'httponly' => true,
    ];
  }

  private function createToken() {
    return $this->userRepo->getCurrent()
      ->createToken('Personal-access-token')
      ->accessToken;
  }

  private function clearToken() {
    request()->user()->token()->revoke();
    return Cookie::forget('_token');
  }

  private function attemptedResponse() {
    $currentUser = $this->userRepo->getCurrent();
    $token = $this->createToken();
    $cookie = $this->getCookieDetails($token);
    $res = [
      'user' => $currentUser,
      'token' => $token,
    ];
    return $this->showResponse($res)->cookie(
      $cookie['name'],
      $cookie['value'],
      $cookie['minutes'],
      $cookie['path'],
      $cookie['domain'],
      $cookie['secure'],
      $cookie['httponly']
    );
  }

  public function login() {
    $credentials = [
      'email' => request()->email,
      'password' => request()->password,
    ];
    $attempCredentials = $this->userRepo->checkExist($credentials);
    if (!$attempCredentials) {
      return $this->showResponse('email has not found', true);
    }
    $isLogin = Auth::attempt($attempCredentials);
    if (!$isLogin) {
      return $this->showResponse('email and password has not match', true);
    }
    return $this->attemptedResponse();
  }

  public function logout() {
    $tmpUser = $this->userRepo->getCurrent();
    $cookie = $this->clearToken();
    return $this->showResponse([
      'user' => $tmpUser,
    ])->withCookie($cookie);
  }

  public function register() {
    $reqObj = request()->all();
    $reqObj['password'] = Hash::make($reqObj['password']);
    $roles = $reqObj['roles'] ?? null;
    $this->userRepo->create($reqObj, $roles);
    $attempCredentials = [
      'email' => request()->email,
      'password' => request()->password,
    ];
    Auth::attempt($attempCredentials);
    return $this->attemptedResponse();
  }

  public function profile() {
    $currentUser = $this->userRepo->getCurrent();
    return $this->showResponse($currentUser);
  }

  public function changePassword() {
    $reqObj = request()->all();
    $currentUser = $this->userRepo->getCurrent();
    $user = $this->userRepo->update($currentUser->id, $reqObj);
    if (!$user) return $this->showResponse('error, cannot change password', true);
    return $this->showResponse('success change password, please login with new password');
  }
}
