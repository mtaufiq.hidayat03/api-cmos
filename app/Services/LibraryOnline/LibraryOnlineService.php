<?php


namespace App\Services\LibraryOnline;

use App\Models\ECuti;
use App\Repositories\LibraryOnline\LibraryOnlineRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LibraryOnlineService extends BaseService
{
  protected $libraryOnlineRepo;

  public function __construct(LibraryOnlineRepository $libraryOnlineRepo) {
    $this->libraryOnlineRepo = $libraryOnlineRepo;
  }

  public function getAllLibrary() {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAll(), $columnSorts, $columnFilters);
  }

  public function getAllLibraryByUser($userId) {
    $columnSorts = ['status', 'created_at'];
    $columnFilters = ['remark'];
    $user = Auth::user();

    if($user->type === 9){
        if($this->libraryOnlineRepo->permissionLibarayOnline()->permission_view_other_department == true){
            return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAll(), $columnSorts, $columnFilters);    
        } else {
            return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAllByUser($userId),  $columnSorts, $columnFilters);
        }
    } else if ($this->libraryOnlineRepo->permissionLibarayOnline() != NULL){
        if($this->libraryOnlineRepo->permissionLibarayOnline()->permission_view_other_department == true){
            return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAll(), $columnSorts, $columnFilters);
        } else {
            return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAllByUser($userId),  $columnSorts, $columnFilters);
        }
    }
    return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAllByUser($userId),  $columnSorts, $columnFilters);
    // return $userId;
  }

  public function getLibraryById($id){
    return $this->showResponse($this->libraryOnlineRepo->queryGetById($id));
  }

  public function downloadLibraryOnlineById($id){
    
    return $this->showResponse($this->libraryOnlineRepo->downloadById($id));
  }

  public function getAllLibraryHistory($id){
    return $this->showResponsePaginate($this->libraryOnlineRepo->queryGetAllHistory($id));
  }

}