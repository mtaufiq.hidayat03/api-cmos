<?php

namespace App\Services;

use App\Repositories\AnnouncementRepository;

class AnnouncementService extends BaseService {
  protected $announcementRepo;

  public function __construct(AnnouncementRepository $announcementRepo) {
    $this->announcementRepo = $announcementRepo;
  }

  public function getAll() {
    $search = request()->search;
    $columnSorts = ['status', 'expired', 'title'];
    $columnFilters = ['title', 'content'];
    return $this->showResponsePaginate($this->announcementRepo->queryGetAll($search), $columnSorts, $columnFilters);
  }

  public function get($id) {
    $announcement = $this->announcementRepo->get($id);
    if (!$announcement) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($announcement);
  }

  public function update($id) {
    $values = request()->all();
    $announcement = $this->announcementRepo->update($id, $values);
    if (!$announcement) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($announcement);
  }

  public function destroy($id) {
    $announcement = $this->announcementRepo->destroy($id);
    if (!$announcement) {
      return $this->showResponse('data not found', true);
    }
    return $this->showResponse($announcement);
  }
}
