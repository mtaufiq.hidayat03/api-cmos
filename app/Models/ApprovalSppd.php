<?php


namespace App\Models;

use App\BaseModel;

class ApprovalSppd extends BaseModel
{
  protected $hidden = [];

  protected $table = 'approval_sppd';

  public $timestamps = false;

}
