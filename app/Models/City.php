<?php


namespace App\Models;

use App\BaseModel;
use DateTimeInterface;

class City extends BaseModel
{
  protected $hidden = [];

  protected $table = 'cities';

  public $timestamps = false;

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }
  

}
