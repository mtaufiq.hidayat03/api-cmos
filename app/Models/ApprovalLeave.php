<?php


namespace App\Models;

use App\BaseModel;

class ApprovalLeave extends BaseModel
{
  protected $hidden = [];

  protected $table = 'approval_leave';

  public $timestamps = false;

}
