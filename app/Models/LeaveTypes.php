<?php


namespace App\Models;

use App\BaseModel;

class LeaveTypes extends BaseModel
{
  protected $hidden = [];

  protected $table = 'leave_types';

  public $timestamps = false;

}
