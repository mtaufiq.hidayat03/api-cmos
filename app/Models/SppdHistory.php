<?php


namespace App\Models;

use App\BaseModel;
use DateTimeInterface;

class SppdHistory extends BaseModel
{
  protected $hidden = [];

  protected $table = 'sppd_histories';

  public $timestamps = false;

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  protected $guarded = [
    'id', 'created_at'
  ];

  protected $casts = [
    'created_at' => 'datetime'
  ];

}
