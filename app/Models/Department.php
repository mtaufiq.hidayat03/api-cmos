<?php


namespace App\Models;

use App\BaseModel;
use App\User;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class Department extends BaseModel
{
  protected $guarded = [
    'id', 'created_at', 'updated_at'
  ];

  protected $hidden = [];

  protected $table = 'departments';

  
  
}
