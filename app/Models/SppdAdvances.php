<?php


namespace App\Models;

use App\BaseModel;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class SppdAdvances extends BaseModel
{
//   protected $guarded = [
//     'id', 'created_at', 'updated_at'
//   ];

  protected $casts = [
    'created_at' => 'datetime',
    'updated_at' => 'datetime'
  ];

  protected $table = 'sppd_advances';

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }
  public function scopeQueryAll() {
    return $this->with([]);
  }
  
  public function sppd() {
    return $this->belongsTo(Sppd::class);
  }
  
}
