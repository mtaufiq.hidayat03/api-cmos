<?php


namespace App\Models;


use App\BaseModel;

class LeaveBalance extends BaseModel
{

    protected $table = 'leave_balances';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'balance_count', 'created_at', 'created_by', 'updated_at', 'updated_by', 'year'];

    public $timestamps = false;

}
