<?php


namespace App\Models;

use App\BaseModel;
use App\Models\ECuti;
use App\User;
use Illuminate\Database\Eloquent\Model;

class LeaveApproval extends BaseModel
{
    protected $table = 'leave_approvals';
    
    protected $fillable =['last_approval', 'last_approval_at', 'step', 'status', 'leave_id'];

    public $timestamps = false;

    public function leave() {
        return $this->belongsTo(ECuti::class)->with(['approver']);
      }

      public function approver() {
        return $this->belongsTo(User::class, 'last_approval', 'id')->select(['id', 'name']);
      }
}
