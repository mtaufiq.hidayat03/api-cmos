<?php


namespace App\Models;

use App\BaseModel;
use DateTimeInterface;
use App\User;

class LeaveNotes extends BaseModel
{
  protected $table = 'leave_notes';

  protected $fillable =['leave_id', 'user_id', 'created_at', 'notes'];

  public $timestamps = false;

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function leave() {
    return $this->belongsTo(ECuti::class)->with(['approver']);
  }

  public function approver() {
    return $this->belongsTo(User::class, 'user_id', 'id')->select(['id', 'name']);
  }
}
