<?php


namespace App\Models;

use App\BaseModel;
use App\User;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class ECuti extends BaseModel
{
  // protected $casts = [
  //   'status' => 'boolean',
  // ];
  protected $casts = [
    'created_at' => 'datetime',
    'updated_at' => 'datetime'
  ];

  protected $guarded = [
    'id', 'created_at', 'updated_at'
  ];

  protected $hidden = [];

  protected $table = 'leaves';
  
  // public $timestamps = false;
  
  // public static function boot() {
  //   parent::boot();
  //   static::creating(function ($model) {
  //     $model->created_at = \Carbon\Carbon::now();
  //   });
  // }

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater', 'leave_type','last_approver', 'notes']);
  }

  public function leave_type() {
    return $this->belongsTo(LeaveTypes::class, 'leave_type_id', 'id')->select(['id', 'name']);
  }

  public function last_approver() {
    return $this->belongsTo(User::class, 'position', 'id')->select(['id', 'name']);
  }

  public function approval() {
    return $this->hasMany(LeaveApproval::class, 'leave_id','id')->with(['approver'])->orderBy('last_approval_at', 'desc');
  }

  public function notes() {
    return $this->hasMany(LeaveNotes::class, 'leave_id','id')->with(['approver'])->orderBy('created_at', 'desc');
  }


 
}
