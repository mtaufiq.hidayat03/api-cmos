<?php


namespace App\Models;

use App\BaseModel;
use DateTimeInterface;

class SppdHotels extends BaseModel
{
//   protected $guarded = [
//     'id', 'created_at', 'updated_at'
//   ];

  protected $hidden = [];

  protected $table = 'sppd_hotels';

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function scopeQueryAll() {
    return $this->with([]);
  }
  
  public function sppd() {
    return $this->belongsTo(Sppd::class);
  }
}
