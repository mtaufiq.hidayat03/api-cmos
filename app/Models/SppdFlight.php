<?php


namespace App\Models;

use App\BaseModel;
use App\Models\Sppd;
use App\User;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class SppdFlight extends BaseModel
{
  protected $guarded = [
    'id', 'created_at', 'updated_at'
  ];

  protected $hidden = [];

  protected $table = 'sppd_flights';

  public function scopeQueryAll() {
    return $this->with(['from', 'to']);
  }

  public function sppd() {
    return $this->belongsTo(Sppd::class)->with(['from','to']);
  }

  public function from(){
    return $this->belongsTo(City::class, 'from');
  }
  
  public function to(){
    return $this->belongsTo(City::class, 'to');
  }
  
}
