<?php


namespace App\Models;

use App\BaseModel;
use App\User;
use App\Models\SppdFlight;
use App\Models\SppdHotels;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class Sppd extends BaseModel
{
  // protected $casts = [
  //   'status' => 'boolean',
  // ];
  protected $casts = [
    'created_at' => 'datetime',
    'updated_at' => 'datetime'
  ];

  protected $guarded = [
    'id', 'created_at', 'updated_at'
  ];

  protected $hidden = [];

  protected $table = 'sppd_transactions';
  
  // public $timestamps = false;
  
  // public static function boot() {
  //   parent::boot();
  //   static::creating(function ($model) {
  //     $model->created_at = \Carbon\Carbon::now();
  //   });
  // }

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater', 'last_approver', 'advances','hotel', 'transport', 'job_area']);
  }

  public function leave_type() {
    return $this->belongsTo(LeaveTypes::class, 'leave_type_id', 'id')->select(['id', 'name']);
  }

  public function last_approver() {
    return $this->belongsTo(User::class, 'position', 'id')->select(['id', 'name']);
  }

  public function creator() {
    return $this->belongsTo(User::class, 'user_id', 'id')->select(['id', 'name']);
  }

  public function hotel() {
    return $this->hasMany(SppdHotels::class);
  }

  public function transport() {
    return $this->hasMany(SppdFlight::class)->with(['from','to']);
  }

  public function advances() {
    return $this->hasMany(SppdAdvances::class);
  }

  public function job_area() {
    return $this->belongsTo(City::class, 'job_area');
  }

 
}
