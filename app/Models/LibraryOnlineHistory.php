<?php


namespace App\Models;

use App\BaseModel;
use App\User;
use App\Models\Company;
use App\Models\Department;
use DateTimeInterface;
use Spatie\Permission\Models\Role as ModelsRole;

class LibraryOnlineHistory extends BaseModel
{
  // protected $casts = [
  //   'status' => 'boolean',
  // ];
  protected $casts = [
    'created_at' => 'datetime',
    'updated_at' => 'datetime'
  ];

  protected $guarded = [
    'created_at', 'updated_at'
  ];

  protected $hidden = [];

//   protected $primaryKey = 'library_id';

  protected $table = 'library_online_history';

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater', 'company', 'department']);
  }

  public function creator() {
    return $this->belongsTo(User::class, 'created_by', 'id')->select(['id', 'name']);
  }

  public function company() {
    return $this->belongsTo(Company::class, 'company_id', 'id')->select(['id', 'name']);
  }

  public function department() {
    return $this->belongsTo(Department::class, 'department_id', 'id')->select(['id', 'name']);
  }

 
}
