<?php

namespace App\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;

class AppServiceProvider extends ServiceProvider {

  private function searchableEloquent() {
    Builder::macro('whereLike', function ($attributes, string $searchTerm) {
      $this->where(function (Builder $query) use ($attributes, $searchTerm) {
        foreach (Arr::wrap($attributes) as $attribute) {
          $query->when(
            str_contains($attribute, '.'),
            function (Builder $query) use ($attribute, $searchTerm) {
              [$relationName, $relationAttribute] = explode('.', $attribute);
              $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $searchTerm) {
                // For Postgres
                $query->where($relationAttribute, 'ILIKE', "%{$searchTerm}%");
                // For MySQL
                // $searchTerm = strtolower($searchTerm);
                // $query->where(DB::raw("lower($relationAttribute)"), "like", "%" . $searchTerm . "%");
              });
            },
            function (Builder $query) use ($attribute, $searchTerm) {
              // For Postgres
              $query->orWhere($attribute, 'ILIKE', "%{$searchTerm}%");
              // For MySQL
              // $searchTerm = strtolower($searchTerm);
              // $query->where(DB::raw("lower($attribute)"), "like", "%" . $searchTerm . "%");
            }
          );
        }
      });
      return $this;
    });
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    if ($this->app->environment() !== 'production') {
      $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
    }
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    $this->searchableEloquent();
    // For ID Carbon (diffHumanReadable dsb.)
    // Carbon::setLocale('id');
    // date_default_timezone_set('Asia/Jakarta');
    // Example :
    // Without locale, the output gonna be like this
    // Carbon\Carbon::parse('2019-03-01')->format('d F Y'); //Output: "01 March 2019"
    // With locale
    // Carbon\Carbon::parse('2019-03-01')->translatedFormat('d F Y'); //Output: "01 Maret 2019"
  }
}
