<?php

namespace App;

class Item extends BaseModel {
  protected $casts = [];
  protected $table = 'master_items';

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
