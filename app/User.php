<?php

namespace App;

use DateTimeInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use App\Notifications\ResetPassword; // Or the location that you store your notifications (this is default).

class User extends Authenticatable {
  use HasApiTokens, HasRoles, Notifiable;
  protected $guard_name = 'web';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $guarded = [
    'id', 'created_at', 'updated_at', 'photo'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'pivot', 'deleted_at'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'notification_email' => 'boolean'
  ];
  protected $dates = [];
  public function getFullNameAttribute() {
    return $this->first_name . ' ' . $this->last_name;
  }

  protected function serializeDate(DateTimeInterface $date): string {
    return $date->format('Y-m-d H:i:s');
  }

  // Email / Notif for reset and forgot password
  public function sendPasswordResetNotification($token) {
    $this->notify(new ResetPassword($token, $this->email));
  }

  public function role() {
    return $this->belongsToMany(Role::class, 'role');
  }

  public function location() {
    return $this->belongsTo(Location::class);
  }
}
