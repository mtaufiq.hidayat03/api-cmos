<?php

namespace App;

use App\Location;

class Category extends BaseModel {
  protected $casts = [];
  protected $table = 'master_item_categories';

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
