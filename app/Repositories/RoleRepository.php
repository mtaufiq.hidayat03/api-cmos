<?php

namespace App\Repositories;

use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class RoleRepository extends BaseRepository {
  protected $role;

  public function __construct(Role $role) {
    $this->role = $role;
  }

  public function queryGetAll() {
    return Role::class;
  }

  public function getAll() {
    $role = Role::get();
    return $role;
  }

  public function get($id) {
    $role = Role::whereId($id)->with(['permissions'])->first();
    return $role;
  }
}
