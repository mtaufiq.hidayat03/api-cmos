<?php


namespace App\Repositories\LibraryOnline;

use App\Models\LibraryOnline;
use App\Models\LibraryOnlineHistory;
use App\Models\Employee;
use App\Models\SettingLibraryOnline;
use App\Models\ApprovalLeave;
use App\Models\LeaveBalance;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;

class LibraryOnlineRepository extends BaseRepository
{
  protected $ecuti;
  protected $userRepo;

  public function __construct(LibraryOnline $libraryOnline, UserRepository $userRepo) {
    $this->libraryOnline = $libraryOnline;
    $this->userRepo = $userRepo;
  }

  public function queryGetAll() {
      return LibraryOnline::queryAll()->whereStatus(1)->whereDeletedAt(NULL)->orderBy('created_at', 'desc');
  }

  public function queryGetAllByUser() {
    $getDepartment = $this->getEmployee()->department_id;
    $getCompany = $this->getEmployee()->company_id;
    // return $getDepartment;
    return LibraryOnline::queryAll()->whereStatus(1)->whereDepartmentId($getDepartment)->whereCompanyId($getCompany)->whereDeletedAt(NULL)->orderBy('created_at', 'desc');
  }

  public function queryGetAllHistory($id) {
    return LibraryOnlineHistory::queryAll()->whereLibraryId($id)->whereStatus(1)->orderBy('created_at', 'desc');
}

  public function queryGetById($id){
    $data = LibraryOnline::where('library_id','=',$id)->with(['creator', 'updater', 'company', 'department'])->first();
    return $data;
  }

  public function downloadById($id){
    $fileName = $this->getLibraryOnline($id)->file_name;
    $tempImage = tempnam(sys_get_temp_dir(), $fileName);
    copy('https://my-cdn.com/files/image.jpg', $tempImage);

    return response()->download($tempImage, $fileName);

    $data = LibraryOnline::where('library_id','=',$id)->with(['creator', 'updater', 'company', 'department'])->first();
    return $data;
  }

  public function getEmployee(){
    $user = Auth::user();
    $employee = Employee::where('user_id', $user->id)->first();
    if(!$employee){
      return false;
    }
    return $employee;
  }

  public function getLibraryOnline($id){
    $user = Auth::user();
    $libraryOnline = LibraryOnline::where('library_id', $id)->first();
    if(!$libraryOnline){
      return false;
    }
    return $libraryOnline;
  }

  public function permissionLibarayOnline(){
    $user = Auth::user();
    $setting = SettingLibraryOnline::where('user_id', $user->id)->first();
    if(!$setting){
      return false;
    }
    return $setting;
  }



}