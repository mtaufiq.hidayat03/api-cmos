<?php

namespace App\Repositories;

use App\Announcement;

class AnnouncementRepository extends BaseRepository {
  protected $announcement;

  public function __construct(Announcement $announcement) {
    $this->announcement = $announcement;
  }

  public function queryGetAll($search = '') {
    if (!empty($search)) return Announcement::queryAll()->whereLike(['title', 'content', 'expired'], $search);
    return Announcement::queryAll();
  }

  public function getAll() {
    $announcement = Announcement::get();
    return $announcement;
  }

  public function get($id) {
    $announcement = Announcement::whereId($id)->first();
    return $announcement;
  }

  public function create($values, $roles) {
    $announcement = null;
    // DB::transaction(function () use ($values, $roles) {
    //   $announcement = Announcement::create($values);
    //   array_map(function ($roleName) use (&$announcement) {
    //     $announcement->assignRole($roleName);
    //   }, $roles);
    // });
    return $announcement;
  }

  public function update($id, $values) {
    $announcement = Announcement::find($id);
    if ($announcement) {
      // $values['image_url'] = $this->getUploadFile($announcement);
      $values = $this->setUpdater($values);
      $announcement->fill($values);
      $announcement->save();
    }
    return $announcement;
  }

  public function destroy($id) {
    $announcement = Announcement::find($id);
    if (!$announcement) return false;
    $announcement->delete();
    return $announcement;
  }
}
