<?php


namespace App\Repositories\Hr;

use App\Models\ECuti;
use App\Models\LeaveTypes;
use App\Models\ApprovalLeave;
use App\Models\Employee;
use App\Models\LeaveBalance;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;

class ECutiRepository extends BaseRepository
{
  protected $ecuti;
  protected $userRepo;

  public function __construct(ECuti $ecuti, UserRepository $userRepo) {
    $this->ecuti = $ecuti;
    $this->userRepo = $userRepo;
  }

  public function queryGetAll() {
      return ECuti::queryAll();
  }

  public function queryGetAllByUser($userId) {
     return ECuti::queryAll()->whereCreatedBy($userId)->orderBy('created_at', 'desc');
  }

  public function get($idcuti) {
    $data = ECuti::where('id','=',$idcuti)->with(['creator'])->first();
    return $data;
    // return ECuti::queryAll();
  }

  public function update($id, $values, $status) {
    $ecuti = ECuti::find($id);
    if ($ecuti) {
      $values = $this->setCreatorUpdater($values);

      $from = new Carbon ($values['from']);
      $to = new Carbon ($values['to']);
      $count_days = ($to->diffInDays($from))+1;
      
  
      //Weekend Check
      $minusDay = 0;
      $format_date_from = date_create($from);
      $format_date_to = date_create($to);
  
      $start = date_format($format_date_from, "Y/m/d");
      $end = date_format($format_date_to, "Y/m/d");
  
      for($i = $start; $i <= $end; $i++){
          if((date('N', strtotime($i)) >= 6) == false){
              $minusDay += 0;
          } else {
              $minusDay += 1;
          }
      }
  
      // Minus Weekend
      $total_days = $count_days - $minusDay;
  
  
      $values['count_days'] = $total_days;
      $values['status'] = $status;
      $ecuti->fill($values);
      $ecuti->save();
    }
    return $ecuti;
  }

  public function destroy($id) {
    $ecuti = ECuti::find($id);
    if (!$ecuti) return false;
    $ecuti->delete();
    return $ecuti;
  }

  public function queryGetLeaveTypes(){
    return LeaveTypes::get();
  }

  public function create($values, $status, $firstApproval) {
    $user = Auth::user();
    $employee = Employee::where('user_id', $user->id)->first();
    $leaveBalance = LeaveBalance::where('user_id', $user->id)->orderBy('expired_date', 'desc')->first();

    $values = $this->setCreator($values);
    $from = new Carbon ($values['from']);
    $to = new Carbon ($values['to']);
    $count_days = ($to->diffInDays($from))+1;

    //Weekend Check
    $minusDay = 0;
    $format_date_from = date_create($from);
    $format_date_to = date_create($to);

    $start = date_format($format_date_from, "Y/m/d");
    $end = date_format($format_date_to, "Y/m/d");

    for($i = $start; $i <= $end; $i++){
        if((date('N', strtotime($i)) >= 6) == false){
            $minusDay += 0;
        } else {
            $minusDay += 1;
        }
    }

    // Minus Weekend
    $total_days = $count_days - $minusDay;


    $values['count_days'] = $total_days;
    $values['user_id'] = $user->id;
    $values['status'] = $status;
    $values['position'] = $firstApproval;

    if(empty($values['remark'])){
      $values['remark'] = '-';
    } 

    $eCuti = ECuti::create($values);
    $eCuti->save();
    return $eCuti;
  }
  
  public function checkApproval(){
    $user = Auth::user();
    $approval = ApprovalLeave::where('user_id', $user->id)->first();
    if(!$approval){
      return false;
    }
    return true;
  }

  public function getSelectedApproval(){
    $user = Auth::user();
    $approval = ApprovalLeave::find($user->user_id);
    if($approval){
      return $approval;
    }
    return "Data tidak ditemukan";
  }

  public function leaveBalance(){
    $user = Auth::user();
    $checkLeaveBalance = LeaveBalance::where('user_id', $user->id)->orderBy('expired_date', 'desc')->first();
    if($checkLeaveBalance){
      return $checkLeaveBalance;
    }
    return false;
  }

  public function getLeaveAccept(){
    $user = Auth::user();
    $year = Carbon::now()->format('Y');
    $sql = "select sum(leaves.count_days) as count from leaves where leaves.user_id =".$user->id." and leaves.status in (1,2,4) and date_part('YEAR',leaves.created_at)='".$year."'";

    return DB::select(DB::raw($sql));
  }

  
  function getApproval($step)
    {
      $user = Auth::user();
      $sql = "SELECT *, (a_approval ->> 'step') as step, (a_approval ->> 'approval_id') as approval_id FROM approval_leave a
      CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
      LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
      WHERE a.user_id = '$user->id' and (a_approval ->> 'step') = '$step'";

      return DB::select(DB::raw($sql));

    }

    // public function getLeaveBalance(){
    //   $user = Auth::user();
    //   $checkLeaveBalance = LeaveBalance::where('user_id', $user->id)->where('year', Carbon::now()->format('Y'))->first();
    //   if($checkLeaveBalance){
    //     $currentUser = $this->userRepo->getCurrent();
    //     $cutiUsed = $this->getLeaveAccept()[0]->count ? $this->getLeaveAccept()[0]->count : 0;
    //     $data = $currentUser;
    //     $data['total_cuti'] = $checkLeaveBalance->balance_count;
    //     $data['used'] = $cutiUsed;
    //     $data['balance'] = $checkLeaveBalance->balance_count - $cutiUsed;
    //     return $data;
    //   }
    //   return false;
    // }

    public function getLeaveBalance(){
      $user = Auth::user();
      $employee = Employee::where('user_id', $user->id)->first();
      $leaveBalance = LeaveBalance::where('user_id', $user->id)->orderBy('expired_date', 'desc')->first();
      if(!$leaveBalance){
        $data['total_cuti'] = 0;
        return $data;
      }
      $newDateExp = Carbon::parse($leaveBalance->expired_date)->subYear(1)->format('Y-m-d');
      $count = $leaveBalance->balance_count - countApprovedLeaveBalance($leaveBalance->user_id, $leaveBalance->expired_date, 1)[0]->count - countMassLeaveByYearAfterInOffice($employee->start_date) - countLastYearOutstandingLeave($leaveBalance->user_id, 4, $newDateExp, 1)[0]->sum;
      if($leaveBalance){
        $currentUser = $this->userRepo->getCurrent();
        $cutiUsed = $this->getLeaveAccept()[0]->count ? $this->getLeaveAccept()[0]->count : 0;
        $data = $currentUser;
        $data['total_cuti'] = $leaveBalance->balance_count;
        $data['used'] =  $leaveBalance->balance_count - $count;
        $data['balance'] = $count;
        return $data;
      }
      return false;
    }

    public function checkEmployee(){
      $user = Auth::user();
      $employee = Employee::where('user_id', $user->id)->first();
      if($employee){
        return $employee;
      }
      return false;
    }
}
