<?php


namespace App\Repositories\Hr;


use App\Models\ApprovalSppd;
use App\Models\Sppd;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApprovalSppdRepository extends BaseRepository
{
    protected $sppd;

    public function __construct(Sppd $sppd) {
        $this->sppd = $sppd;
    }

    public function queryGetAll() {
        return Sppd::queryAll()->wherePublish(1)->whereStatus(1)->orWhere('status',2);
    }  

    public function queryGetAllByApprover($userId) {
        $query = Sppd::queryAll()->wherePosition($userId)
        ->wherePublish(1)
        ->where(function($q){
            $q->whereStatus(1)
              ->orWhere('status','=',2);
        });
        return $query;
        
    }

    public function get($sppdId) {
        $data = Sppd::whereId($sppdId)->with(['creator', 'updater', 'last_approver', 'advances','hotel', 'transport'])->first();
        return $data;
        // return sppd::queryAll();
    }

    public function approveAction($id, $status, $nextApproval, $user_id) {
        $sppd = Sppd::find($id);

        $values['status'] = $status;
        $values['position'] = $nextApproval;
        if($status == 2){
            $getStepApproval = $this->getStepApprovalSppd($user_id)[0]->step;
            $values['step'] = $getStepApproval+1;
        }
        $sppd->fill($values);
        $sppd->save();

        return $sppd;
    }

    public function getSelectedApproval($userId){
        $approval = ApprovalSppd::get()->where('user_id',$userId);
        if($approval){
            return $approval[0]->total_approval;
            // return "bisa";
        }
        return "Data tidak ditemukan";
      }

    public function getStepApprovalSppd($user_id) {
        $user = Auth::user();
        $sql = "SELECT (a_approval ->> 'step') as step FROM approval_sppd a
            CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
            LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
            WHERE a.user_id = '$user_id'"." and (a_approval ->> 'approval_id') ='".$user->id."'";
        
        return DB::select( DB::raw($sql));
    }

    function getApprovalSppd($user_id, $step)
    {
        $sql = "SELECT *, (a_approval ->> 'step') as step, (a_approval ->> 'approval_id') as approval_id FROM approval_sppd a
        CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
        LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
        WHERE a.user_id = '$user_id' and (a_approval ->> 'step') = '$step'";

        return DB::select(DB::raw($sql));

    }

}