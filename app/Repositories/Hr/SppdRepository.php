<?php


namespace App\Repositories\Hr;

use App\Models\Sppd;
use App\Models\ApprovalSppd;
use App\Models\SppdAdvances;
use App\Models\SppdFlight;
use App\Models\SppdHistory;
use App\Models\SppdHotels;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use PDF;

class SppdRepository extends BaseRepository
{
  protected $Sppd;
  protected $userRepo;

  public function __construct(Sppd $Sppd, UserRepository $userRepo) {
    $this->Sppd = $Sppd;
    $this->userRepo = $userRepo;
  }

  public function queryGetAll() {
      return Sppd::queryAll()->orderBy('created_at','desc');
  }

  public function queryGetAllByUser($userId) {
     return Sppd::queryAll()->whereUserId($userId)->orderBy('created_at','desc');
  }

  public function getSppdById($sppdId){
      return Sppd::where('id','=',72)->with(['creator', 'updater', 'last_approver', 'advances','hotel', 'transport', 'job_area'])->first();
    //   return $sppdId;
  }

  public function createSppd($values, $status){
    $user = Auth::user();
    $countSppd = $this->countSppd();
    $num = sprintf("%'.03d", $countSppd + 1) ;
    $createDocNo = "SPPD/CMI/".$this->getRomawi(date('m'))."/".date('Y')."/".$num;
    $getFirstApproval = $this->getApprovalSppd(1)[0]->approval_id;

    $values['user_id'] = $user->id;
    $values['doc_no'] = $createDocNo;

    if($status == 1){
        $values['publish'] = 1;
        $values['status'] = 1;
        $values['step'] = 1;
        $values['position'] = $getFirstApproval;
    } elseif($status == 5){
        $values['status'] = 5;
        $values['publish'] = 0;
    } else {
        $values['status'] = 0;
        $values['publish'] = 0;
    }

    // return $values;
    $sppd = Sppd::create($values);
    // $sppd->save();

     //Sppd history
     $valuesHistory['sppd_id'] = $sppd->id;
     $valuesHistory['position'] = $getFirstApproval;
     $valuesHistory['type'] = "insert";
     $valuesHistory['created_at'] = Carbon::now();

     $sppdHistory = SppdHistory::insert($valuesHistory);
    //  $sppdHistory->save();
     return $sppd;
  }


  public function updateSppd($sppdId, $status, $values, $type) {
    $sppd = Sppd::Find($sppdId);
    $getFirstApproval = $this->getApprovalSppd(1)[0]->approval_id;

    if($status != 0){
        $countSppd = $this->countSppd();
        $num = sprintf("%'.03d", $countSppd + 1);
        $createDocNo = "SPPD/CMI/".$this->getRomawi(date('m'))."/".date('Y')."/".$num;
        $getFirstApproval = $this->getApprovalSppd(1)[0]->approval_id;

        $values['doc_no'] = $createDocNo;
    }
    

    if($type == 'all'){
        if($status == 1){
            $values['publish'] = 1;
            $values['status'] = 1;
            $values['step'] = 1;
            $values['position'] = $getFirstApproval;
        } else {
            $values['status'] = 0;
            $values['publish'] = 0;
        }
    } else {
        if($status == 1){
            $values['publish'] = 1;
            $values['status'] = 1;
            $values['step'] = 1;
            $values['position'] = $getFirstApproval;
        } else {
            $values['status'] = 0;
            $values['publish'] = 0;
        }
    }

    $sppd->fill($values);
    $sppd->save();
    return $sppd;
  }


  
  public function createSppdAdvance($values, $sppdId){
    $values['sppd_id'] = $sppdId;
    // return $values;
    $sppd = SppdAdvances::create($values);
    $sppd->save();
    return $sppd;
  }
  

  public function createSppdHotel($values, $sppdId){
    $values['sppd_id'] = $sppdId;
    // return $values;
    $sppd = SppdHotels::create($values);
    $sppd->save();
    return $sppd;
  }

  public function createSppdFlight($values, $sppdId){
    $values['sppd_id'] = $sppdId;
    $sppd = SppdFlight::create($values);
    $sppd->save();
    return $sppd;
  }

  public function destroy($id) {
    $sppd = Sppd::find($id);
    $sppdAdvances = SppdAdvances::where('sppd_id','=', $id);
    $sppdHotels = SppdHotels::where('sppd_id','=', $id);
    $sppdFlights = SppdFlight::where('sppd_id','=',$id);

    if (!$sppd) return false;
    $sppd->delete();    
    $sppdAdvances->delete();
    $sppdFlights->delete();
    $sppdHotels->delete();
    return $sppd;
  }

  public function countSppd(){
      $getCount = Sppd::wherePublish(1)->count();
      return $getCount;
  }

  public function checkApproval(){
    $user = Auth::user();
    $approval = ApprovalSppd::where('user_id', $user->id)->first();
    if(!$approval){
      return false;
    }
    return true;
  }

  public function getRomawi($bln){
        switch ($bln){
                case 1: 
                    return "I";
                    break;
                case 2:
                    return "II";
                    break;
                case 3:
                    return "III";
                    break;
                case 4:
                    return "IV";
                    break;
                case 5:
                    return "V";
                    break;
                case 6:
                    return "VI";
                    break;
                case 7:
                    return "VII";
                    break;
                case 8:
                    return "VIII";
                    break;
                case 9:
                    return "IX";
                    break;
                case 10:
                    return "X";
                    break;
                case 11:
                    return "XI";
                    break;
                case 12:
                    return "XII";
                    break;
        }
    }


    function getApprovalSppd($step)
    {
        $user = Auth::user();
        $sql = "SELECT *, (a_approval ->> 'step') as step, (a_approval ->> 'approval_id') as approval_id FROM approval_sppd a
        CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
        LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
        WHERE a.user_id = '$user->id' and (a_approval ->> 'step') = '$step'";

        return DB::select(DB::raw($sql));

    }

    //TiCKET
    public function queryGetAllTicket($id) {
        return SppdFlight::queryAll()->whereSppdId($id)->orderBy('created_at','desc');
    }

    public function sppdTicketDestroy($id){
        $sppdFlight = SppdFlight::find($id);
    
        if (!$sppdFlight) return false;
        $sppdFlight->delete();
        return $sppdFlight;
    }

    public function sppdTicketUpdate($values, $id){
        $sppdFlight = SppdFlight::Find($id);
        $sppdFlight->fill($values);
        $sppdFlight->save();
        return $sppdFlight;
    }

    //Hotel
    public function queryGetAllHotel($id) {
        return SppdHotels::queryAll()->whereSppdId($id)->orderBy('created_at','desc');
    }

    public function sppdHotelDestroy($id){
        $sppdHotel = SppdHotels::find($id);
    
        if (!$sppdHotel) return false;
        $sppdHotel->delete();
        return $sppdHotel;
    }

    public function sppdHotelUpdate($values, $id){
        $sppdHotel = SppdHotels::Find($id);
        $sppdHotel->fill($values);
        $sppdHotel->save();
        return $sppdHotel;
    }

    //Advances
    public function queryGetAllAdvances($id) {
        return SppdAdvances::queryAll()->whereSppdId($id)->orderBy('created_at','desc');
    }

    public function sppdAdvanceDestroy($id){
        $sppdAdvances = SppdAdvances::find($id);
    
        if (!$sppdAdvances) return false;
        $sppdAdvances->delete();
        return $sppdAdvances;
    }
    public function sppdAdvancesUpdate($values, $id){
        $sppdAdvances = SppdAdvances::Find($id);
        $sppdAdvances->fill($values);
        $sppdAdvances->save();
        return $sppdAdvances;
    }

    public function downloadSppdPdf($id){

        $sppd = DB::table('sppd_transactions')
        ->select('sppd_transactions.*','employees.nik AS employeeNik','employees.name AS employeeName',
        'departments.name as department', 'positions.name as position', 'groups.name as group', 'companies.name as company',
        'employee_details.bank', 'employee_details.bank_branch  AS bankBranch', 'employee_details.bank_account_number AS bankNumber', 'employee_details.bank_account_name  AS bankAccount','banks.name AS bankName'
        )
        ->leftjoin('employees','employees.user_id','=','sppd_transactions.user_id')
        ->leftjoin('employee_details','employees.id','=','employee_details.employee_id')
        ->leftjoin('banks','banks.id','=','employee_details.bank')
        ->leftJoin('companies', 'companies.id', '=', 'employees.company_id')
        ->leftJoin('departments', 'departments.id', '=', 'employees.department_id')
        ->leftJoin('positions', 'positions.id', '=', 'employees.position_id')
        ->leftJoin('groups', 'groups.id', '=', 'employees.group_id')
        ->where('sppd_transactions.id',$id)
        ->first();

        $sppd_advance = SppdAdvances::where('sppd_id',$id)->get();
        $sppd_hotel   = SppdHotels::where('sppd_id',$id)->get();
        $sppd_flight  = DB::table('sppd_flights')
        ->select('sppd_flights.*','a.name AS cityFrom', 'b.name AS cityTo')
        ->leftJoin('cities AS a', 'a.id', '=', 'sppd_flights.from')
        ->leftJoin('cities AS b', 'b.id', '=', 'sppd_flights.to')
        ->where('sppd_flights.sppd_id',$id)
        ->get();
        $sppd_history = SppdHistory::where('sppd_id',$id)->get();
        $filename='SPPD_'.ucwords(strtolower($sppd->employeeName)).'_'.date('m').'_'.date('Y').'.pdf';

        $data['city'] = $this->getCity($sppd->job_area);
        $data['sppd'] = $sppd;
        $data['sppd_flight'] = $sppd_flight;
        $data['sppd_hotel'] = $sppd_hotel;
        $data['sppd_advance'] = $sppd_advance;
        $data['sppd_history'] = $sppd_history;
        $pdf = PDF::loadView('sppd_pdf', $data);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->download($filename);
    }

    function getCity($cityID = null)
    {
        $city = explode(",",$cityID);
        $sql = DB::table('cities')
        ->select('*')
        ->whereIn('id', $city)
        ->get();

        foreach ($sql as $val){
            $cityName[] = $val->name;
        }
        return implode(", ",$cityName);
    }

}