<?php


namespace App\Repositories\Hr;

use App\Models\City;
use App\Repositories\BaseRepository;

class CityRepository extends BaseRepository
{
  protected $city;

  public function __construct(City $city) {
    $this->city = $city;
  }

  public function queryGetCity($search = '') {
    if (!empty($search)) return City::get();
      return City::get();
  }

}
