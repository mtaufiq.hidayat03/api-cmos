<?php


namespace App\Repositories\Hr;

use App\Models\ECuti;
use App\Models\LeaveNotes;
use App\Models\LeaveTypes;
use App\Models\ApprovalLeave;
use App\Models\LeaveApproval;
use App\Models\LeaveBalance;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ApprovalECutiRepository extends BaseRepository
{
    protected $ecuti;

    public function __construct(ECuti $ecuti) {
        $this->ecuti = $ecuti;
    }

    public function queryGetAll() {
        return ECuti::queryAll()->whereStatus(1)->orWhere('status',2);
    }  

    public function queryGetAllByApprover($userId) {
        $query = ECuti::queryAll()->wherePosition($userId)->where(function($q){
            $q->whereStatus(1)
              ->orWhere('status','=',2);
        });
        // $query = ECuti::queryAll()->wherePosition($userId)->whereStatus(1)->orWhere('status','=',2);
          
        return $query;
        
    }

    public function get($idcuti) {
        $data = ECuti::whereId($idcuti)->with(['creator'])->first();
        return $data;
        // return ECuti::queryAll();
    }

    public function approveAction($id, $status, $nextApproval) {
        $valuesForm = request()->all();
        $user = Auth::user();

        $ecuti = ECuti::find($id);
        $values['status'] = $status;
        $values['position'] = $nextApproval;
        $ecuti->fill($values);
        $ecuti->save();

        // Notes
        if($status == 3 || $status == 4 || $status == 2){
            $valuesNotes['leave_id'] = $id;
            $valuesNotes['user_id'] = $user->id;
            $valuesNotes['notes'] = $valuesForm['notes'];
            $valuesNotes['created_at'] = Carbon::now();
            LeaveNotes::insert($valuesNotes);
        }

        if($status == 3){
            $statusApproval = 'reject';
            $valuesApproval['leave_id'] = $id;
            $valuesApproval['last_approval'] = $user->id;
            $valuesApproval['last_approval_at'] = Carbon::now();
            $valuesApproval['step'] = $this->getStepApprovalEcuti($ecuti->user_id)[0]->step;
            $valuesApproval['status'] = $statusApproval;
            $eCutiApproval = LeaveApproval::create($valuesApproval);
            $eCutiApproval->save();
        }

        if($status == 4 || $status == 2){
            $statusApproval = 'accept';
            $valuesApproval['leave_id'] = $id;
            $valuesApproval['last_approval_at'] = Carbon::now();
            $valuesApproval['step'] = $this->getStepApprovalEcuti($ecuti->user_id)[0]->step;
            $valuesApproval['status'] = $statusApproval;
            $valuesApproval['last_approval'] = $user->id;
            $eCutiApproval = LeaveApproval::create($valuesApproval);
            $eCutiApproval->save();
        }

        return $ecuti;
    }

    public function getSelectedApproval($userId){
        $approval = ApprovalLeave::get()->where('user_id',$userId);
        if($approval){
            return $approval[0]->total_approval;
            // return "bisa";
        }
        return "Data tidak ditemukan";
      }

    public function getStepApprovalEcuti($user_id) {
        $user = Auth::user();
        $sql = "SELECT (a_approval ->> 'step') as step FROM approval_leave a
            CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
            LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
            WHERE a.user_id = '$user_id'"." and (a_approval ->> 'approval_id') ='".$user->id."'";
        
        return DB::select( DB::raw($sql));
    }

    function getApprovalEcuti($user_id, $step)
    {
        $sql = "SELECT *, (a_approval ->> 'step') as step, (a_approval ->> 'approval_id') as approval_id FROM approval_leave a
        CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
        LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
        WHERE a.user_id = '$user_id' and (a_approval ->> 'step') = '$step'";

        return DB::select(DB::raw($sql));

    }

}