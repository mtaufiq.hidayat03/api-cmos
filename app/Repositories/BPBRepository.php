<?php

namespace App\Repositories;

use App\BPB;
use App\Location;
use Arr;

class BPBRepository extends BaseRepository {
  protected $bpb;
  private $publishDate;

  public function __construct(BPB $bpb) {
    $this->bpb = $bpb;
    $this->publishDate = date('Y-m-d');
  }

  private function counterDocNo($locationId){
    $currentBpb = $this->getBpbCurrent($locationId);
    $docNum = null;
    if($currentBpb){
      $explodedDocNo = explode('-',$currentBpb->doc_no);
      $docNum = end($explodedDocNo);
    }
    return $this->incrementStringZero($docNum);
  }

  public function getBpbCurrent($locationId) {
    return BPB::queryAll()
      ->whereLocationId($locationId)
      ->wherePublish($this->publishDate)
      ->orderBy('id','desc')
      ->first();
  }

  public function create($values) {
    $locationId = Arr::get($values, 'location_id');
    $location = Location::queryAlias()
      ->whereId($locationId)
      ->select([
        'id',
        'company_id',
        'alias',
      ])
      ->first();
    if (!$location) throw new Exception("location not found for generate");
    $aliasCompany = $location->company->alias;
    $aliasLocation = $location->alias;
    $date = date('dmy');
    $counterNumber = $this->counterDocNo($locationId);
    $docNo = "BPB-$aliasCompany-$aliasLocation-$date-$counterNumber";
    $values['doc_no'] = $docNo;
    $values['publish'] = $this->publishDate;
    $values['status'] = true;
    $values = $this->setCreatorUpdater($values);
    $data = BPB::create($values);
    return $data;
  }
}
