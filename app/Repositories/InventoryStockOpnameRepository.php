<?php

namespace App\Repositories;

use App\InventoryStockOpname;

class InventoryStockOpnameRepository extends BaseRepository {
  protected $inventoryStockOpname;

  public function __construct(InventoryStockOpname $inventoryStockOpname) {
    $this->inventoryStockOpname = $inventoryStockOpname;
  }

  private function calcActualStock($good, $bad) {
    return $good + $bad;
  }

  public function queryGetAll($search = '') {
    if (!empty($search)) return InventoryStockOpname::queryList()->whereLike(['notes'], $search)->orderBy('created_at', 'desc');
    return InventoryStockOpname::queryList();
  }

  public function getAll() {
    $inventoryStockOpname = InventoryStockOpname::get();
    return $inventoryStockOpname;
  }

  public function get($id) {
    $inventoryStockOpname = InventoryStockOpname::whereId($id)->first();
    return $inventoryStockOpname;
  }

  public function queryGetByInventoryId($inventoryId) {
    $inventoryStockOpname = InventoryStockOpname::queryList()
      ->whereInventoryId($inventoryId);
    return $inventoryStockOpname;
  }

  public function getByInventoryId($inventoryId) {
    $inventoryStockOpname = $this->queryGetByInventoryId($inventoryId)->first();
    return $inventoryStockOpname;
  }

  public function create($values) {
    $values = $this->setCreator($values);
    $values['actual_stock'] = $this->calcActualStock($values['good_stock'], $values['bad_stock']);
    $values = $this->setCreator($values);
    $inventoryStockOpname = InventoryStockOpname::create($values);
    $inventoryStockOpname->save();
    return $inventoryStockOpname;
  }

  public function update($id, $values) {
    $inventoryStockOpname = InventoryStockOpname::find($id);
    if ($inventoryStockOpname) {
      $inventoryStockOpname->fill($values);
      $inventoryStockOpname->save();
    }
    return $inventoryStockOpname;
  }

  public function destroy($id) {
    $inventoryStockOpname = InventoryStockOpname::find($id);
    if (!$inventoryStockOpname) return false;
    $inventoryStockOpname->delete();
    return $inventoryStockOpname;
  }
}
