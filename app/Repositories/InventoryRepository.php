<?php

namespace App\Repositories;

use App\Inventory;
use Arr;
use Str;

class InventoryRepository extends BaseRepository {
  protected $inventory;

  public function getStatusInventory($max, $min, $onhand) {
    if ($max == $onhand) {
      return "Max Stock";
    } else if ($onhand >= $max) {
      return "Over Stock";
    } else if ($onhand <= $min) {
      return "Urgent Order";
    } else if ($onhand < $max && $onhand > $min) {
      return "Safe Stock";
    } else {
      return "-";
    }
  }

  public function __construct(Inventory $inventory) {
    $this->inventory = $inventory;
  }

  public function queryGetAll($search = '') {
    if (!empty($search)) return Inventory::queryAll()->whereLike(['product_type', 'kind', 'product.name', 'product.code'], $search);
    return Inventory::queryAll();
  }

  public function getAll() {
    $inventory = Inventory::get();
    return $inventory;
  }

  public function get($id) {
    $inventory = Inventory::whereId($id)->first();
    return $inventory;
  }

  public function getWithQr($uuid) {
    $inventory = Inventory::queryDetail()
        ->whereUuid($uuid)
        ->first();
    return $inventory;
  }

  public function getByProductAndLocation($productId, $locationId){
    $data = Inventory::where([
      'product_id' => $productId,
      'location_id' => $locationId
    ])->first();
    if(!$data) return false;
    return $data;
  }

  public function create($values) {
    $values['uuid'] = Str::uuid();
    $values['stock_status'] = "Over Stock";
    $values = $this->setCreatorUpdater($values);
    $data = Inventory::create($values);
    return $data;
  }

  public function update($id, $values) {
    $inventory = Inventory::find($id);
    if ($inventory) {
      $values = $this->setUpdater($values);
      $inventory->fill($values);
      $inventory->save();
    }
    return $inventory;
  }

  public function destroy($id) {
    $inventory = Inventory::find($id);
    if (!$inventory) return false;
    $inventory->delete();
    return $inventory;
  }
}
