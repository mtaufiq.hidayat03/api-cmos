<?php

namespace App\Repositories;

use App\BPBItem;

class BPBItemRepository extends BaseRepository {
  protected $bpbItem;

  public function __construct(BPBItem $bpbItem) {
    $this->bpbItem = $bpbItem;
  }

  public function create($values) {
    $data = BPBItem::create($values);
    if(!$data) return false;
    return $data;
  }
}
