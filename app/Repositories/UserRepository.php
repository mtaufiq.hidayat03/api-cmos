<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository {
  protected $user;

  public function __construct(User $user) {
    $this->user = $user;
  }

  public function isCurrent($id) {
    $isCurrentUser = $id == $this->getCurrent()->id;
    if ($isCurrentUser) return true;
    return false;
  }

  public function queryGetAll() {
    return User::class;
  }

  public function getAll() {
    $user = User::get();
    return $user;
  }

  public function getCurrent() {
    $user = Auth::user();
    if ($user) {
      $user['roles'] = $this->getRoles($user->id);
    }
    return $user;
  }

  public function get($id) {
    $user = User::whereId($id)->first();
    if ($user) {
      $user['roles'] = $this->getRoles($id);
      $user['permissions'] = $this->getPermissions($id);
    }
    return $user;
  }

  public function create($values, $roles) {
    if (!$roles) $roles = ["Super Administrator"];
    $user = null;
    DB::transaction(function () use ($values, $roles) {
      $user = User::create($values);
      array_map(function ($roleName) use (&$user) {
        $user->assignRole($roleName);
      }, $roles);
    });
    return $user;
  }

  public function update($id, $values) {
    $user = User::find($id);
    if ($user) {
      $values['image_url'] = $this->getUploadFile($user);
      $values = $this->setUpdater($values);
      $newPassword = $values['password'];
      if ($newPassword) $values['password'] = Hash::make($newPassword);
      $user->fill($values);
      $user->save();
    }
    return $user;
  }

  public function destroy($id) {
    $user = User::find($id);
    if (!$user) return false;
    $isCurrentUser = $this->isCurrent($id);
    if ($isCurrentUser) return false;
    $user->delete();
    return $user;
  }

  public function checkExist($credentials) {
    $userEmail = User::whereEmail($credentials['email'])->exists();
    if ($userEmail) {
      return [
        'email' => $credentials['email'],
        'password' => $credentials['password']
      ];
    } else {
      return false;
    }
  }

  public function getPermissions($id) {
    $tmpPermissions =  User::find($id)->getAllPermissions();
    if (!$tmpPermissions) return false;
    return collect($tmpPermissions)->map(function ($obj) {
      return $obj->name;
    });
  }

  public function getRoles($id) {
    $tmpRoles = User::find($id)->roles;
    if (!$tmpRoles) return false;
    return collect($tmpRoles)->map(function ($obj) {
      return $obj->name;
    });
  }

  public function getRolesUser($id) {
    $roles = User::find($id)->roles;
    $name = null;
    foreach($roles as $rls) {
      $name = $rls['name'];
    }
    return $name;
  }
}
