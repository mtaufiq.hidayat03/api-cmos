<?php

namespace App\Repositories;

use App\SPB;
use App\SPBKolis;

class SPBRepository extends BaseRepository {
  protected $spb;
  protected $spbKolis;

  public function __construct(
    SPB $spb,
    SPBKolis $spbKolis
  ) {
    $this->spb = $spb;
    $this->spbKolis = $spbKolis;
  }

  public function setStatusWithSpbKolis($spbId) {
    $spbKolises = SPBKolis::where([
      'spb_id' => $spbId
    ])
      ->select(['id', 'spb_id', 'bpb_status'])
      ->get()->toArray();
    $isDone = true;
    array_map(function ($item) use (&$isDone) {
      $isNotDoneStatus = in_array($item['bpb_status'], SPBKolis::$statusActives);
      if ($isNotDoneStatus) $isDone = false;
    }, $spbKolises);
    if ($isDone) {
      SPB::whereId($spbId)->update(['status' => 3]);
    } else {
      SPB::whereId($spbId)->update(['status' => 2]);
    }
    return $spbKolises;
  }
}
