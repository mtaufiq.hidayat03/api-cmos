<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class BaseRepository {
  protected function incrementStringZero($value = '00000'){
    $intValue = intval($value);
    $intValue++;
    return str_pad($intValue, 5, "0", STR_PAD_LEFT);
  }

  protected function fileUpload($config = [
    'record' => null,
    'property' => null,
    'folder' => 'images',
    'requestName' => 'photo',
    'prefixName' => 'avatar_profile-',
  ]) {
    // Init Object
    $record = $config['record'];
    $property = $config['property'];
    $folder = $config['folder'];
    $requestName = $config['requestName'];
    $prefixName = $config['prefixName'];
    // For Upload
    $id = $record['id'];
    $fileName = $record[$property];
    $file = request()->file($requestName);
    if (request()[$requestName] === 'empty') {
      $fileName = null;
      File::delete($folder . "/" . $record[$property]);
    }
    if ($file) {
      $extension = $file->getClientOriginalExtension();
      $fileName = "$prefixName-$id." . $extension;
      $file->move($folder, $fileName);
    }
    return $fileName;
  }

  protected function getConfigFileUpload($record) {
    return [
      'record' => $record,
      'property' => 'image_url',
      'folder' => 'images',
      'requestName' => 'photo',
      'prefixName' => 'avatar_profile',
    ];
  }

  protected function getUploadFile($record) {
    $configFileUpload = $this->getConfigFileUpload($record);
    return $this->fileUpload($configFileUpload);
  }

  protected function deleteFile($config = [
    'record' => null,
    'property' => null,
    'folder' => 'images',
  ]) {
    $record = $config['record'];
    $property = $config['property'];
    $folder = $config['folder'];
    $fileName = $record[$property];
    File::delete($folder . "/" . $fileName);
    return $fileName;
  }

  protected function setCreatorUpdater($values) {
    $userId = Auth::user()->id;
    $values['created_by'] = $userId;
    $values['updated_by'] = $userId;
    return $values;
  }

  protected function setUpdater($values) {
    $userId = Auth::user()->id;
    $values['updated_by'] = $userId;
    return $values;
  }

  protected function setCreator($values) {
    $userId = Auth::user()->id;
    $values['created_by'] = $userId;
    return $values;
  }
}
