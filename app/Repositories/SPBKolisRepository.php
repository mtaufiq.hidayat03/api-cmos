<?php

namespace App\Repositories;

use App\SPBKolis;
use Illuminate\Support\Facades\DB;

class SPBKolisRepository extends BaseRepository {
  protected $spbKolis;

  public function __construct(SPBKolis $spbKolis) {
    $this->spbKolis = $spbKolis;
  }

  public function getWithQr($uuid) {
    $spbKolis = SPBKolis::queryAllActive()
      ->whereUuid($uuid)->first();
    return $spbKolis;
  }

  public function get($id) {
    $spbKolis = SPBKolis::find($id);
    if (!$spbKolis) return false;
    return $spbKolis;
  }

  public function setBpbStatusByQtyIn($spbKolisId, $qtyInput) {
    $spbKolis = SPBKolis::find($spbKolisId);
    if (!$spbKolis) return false;
    $newQty = 0;
    $bpbStatus = SPBKolis::$statusProgress;
    if($spbKolis->bpb_status == SPBKolis::$statusProgress){
      if ($spbKolis->qty > $qtyInput) {
        $newQty = $spbKolis->qty - $qtyInput;
        $bpbStatus = SPBKolis::$statusPartial;
      } else {
        $newQty = 0;
        $bpbStatus = SPBKolis::$statusDone;
      }
    }else{
      $newQty = $spbKolis->qty_parsial - $qtyInput;
      $bpbStatus = SPBKolis::$statusPartial;
      if($qtyInput >= $spbKolis->qty_parsial) $bpbStatus = SPBKolis::$statusDone;
    }
    DB::table('spb_kolis')->where('id', $spbKolisId)->update([
      'qty_parsial' => $newQty,
      'bpb_status' => $bpbStatus,
    ]);
    $spbKolis = SPBKolis::find($spbKolisId);
    return $spbKolis;
  }
}
