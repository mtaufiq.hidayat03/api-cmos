<?php

namespace App\Repositories;

use App\Location;

class LocationRepository extends BaseRepository {
  protected $location;

  public function __construct(Location $location) {
    $this->location = $location;
  }

  public function queryAll($search = '') {
    if (!empty($search)) return Location::queryAll()->whereLike([], $search);
    return Location::queryAll();
  }

  public function queryAllDropdown() {
    return Location::queryAllDropdown()->get();
  }

  public function getAll() {
    $location = Location::get();
    return $location;
  }

  public function get($id) {
    $location = Location::whereId($id)->first();
    return $location;
  }

  public function getWithQr($uuid) {
    $location = Location::queryDetail()->whereUuid($uuid)->first();
    return $location;
  }

  public function create($values, $roles) {
    $location = null;
    return $location;
  }

  public function update($id, $values) {
    $location = Location::find($id);
    if ($location) {
      // $values['image_url'] = $this->getUploadFile($location);
      $values = $this->setUpdater($values);
      $location->fill($values);
      $location->save();
    }
    return $location;
  }

  public function destroy($id) {
    $location = Location::find($id);
    if (!$location) return false;
    $location->delete();
    return $location;
  }
}
