<?php

namespace App\Repositories;

use App\InventoryHistory;

class InventoryHistoryRepository extends BaseRepository {
  protected $inventoryHistory;

  public function __construct(InventoryHistory $inventoryHistory) {
    $this->inventoryHistory = $inventoryHistory;
  }

  public function setNotes($noPo, $price){
    return json_encode([
      'po' => $noPo,
      'price' => (int) $price
    ]);
  }

  public function getAll() {
    $inventoryHistory = InventoryHistory::get();
    return $inventoryHistory;
  }

  public function get($id) {
    $inventoryHistory = InventoryHistory::whereId($id)->first();
    return $inventoryHistory;
  }

  public function create($values) {
    $data = InventoryHistory::create($values);
    if(!$data) return false;
    return $data;
  }
}
