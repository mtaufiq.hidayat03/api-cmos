<?php

namespace App;

class InventoryStockOpname extends BaseModel {
  protected $casts = [
    'created_at' => 'datetime'
  ];
  protected $table = 'inventory_stock_opname';
  public $timestamps = false;
  public static $showAttributes = [
    'id',
    'actual_stock',
    'good_stock',
    'bad_stock',
    'notes',
    'inventory_id',
    'created_at',
    'created_by',
  ];

  public static function boot() {
    parent::boot();
    static::creating(function ($model) {
      $model->created_at = \Carbon\Carbon::now();
    });
  }

  public function scopeQueryList() {
    return $this->with(['creator', 'inventory','inventory.product'])->select(self::$showAttributes);
  }

  public function scopeQueryListExcel() {
    return $this->with([
      'creator',
      'inventory',
      'inventory.location',
      'inventory.product',
      'inventory.product.item'
    ])->select(self::$showAttributes);
  }

  public function scopeQueryDetail() {
    return $this->with(['creator', 'inventory','inventory.product'])->select(self::$showAttributes);
  }

  public function inventory() {
    return $this->belongsTo(Inventory::class)
      ->select(Inventory::$showAttributes);
  }
}
