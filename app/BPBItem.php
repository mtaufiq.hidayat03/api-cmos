<?php

namespace App;

class BPBItem extends BaseModel {
  public $timestamps = false;
  protected $casts = [];
  protected $table = 'bpb_items';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function bpb() {
    return $this->belongsTo(BPB::class, 'bpb_id');
  }
}
