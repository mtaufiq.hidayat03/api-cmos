<?php

namespace App;

class Announcement extends BaseModel {
  protected $casts = [
    'status' => 'boolean',
    // 'fordepts' => 'boolean',
    // 'forusers' => 'boolean',
  ];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
