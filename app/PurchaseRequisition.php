<?php

namespace App;

class PurchaseRequisition extends BaseModel {
  protected $casts = [];
  protected $table = 'purchase_requisitions';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }
}
