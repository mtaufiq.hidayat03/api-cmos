<?php

namespace App;

class Measure extends BaseModel {
  protected $casts = [];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
