<?php

namespace App;

class Country extends BaseModel {
  protected $casts = [];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
