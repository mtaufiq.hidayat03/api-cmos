<?php

namespace App;

class City extends BaseModel {
  protected $casts = [];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater']);
  }
}
