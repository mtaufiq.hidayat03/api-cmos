<?php

namespace App;

class POItem extends BaseModel {
  protected $casts = [
    'price' => 'decimal:0'
  ];
  protected $table = 'po_items';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function po() {
    return $this->belongsTo(PO::class, 'po_id', 'id');
  }
}
