<?php

namespace App;

class SPB extends BaseModel {
  protected $casts = [];
  protected $table = 'spb';

  public function scopeQueryAll() {
    return $this->with(['creator']);
  }

  public function spbKolises() {
    return $this->hasMany(SPBKolis::class);
  }
}
