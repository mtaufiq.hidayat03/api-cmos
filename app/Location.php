<?php

namespace App;

class Location extends BaseModel {
  protected $casts = [];
  public static $showAttributes = [
    'id',
    'name',
    'alias',
    'address',
    'telp',
    'color',
    'email',
    'email2',
    'created_at',
    'updated_at',
    'city_id',
    'country_id',
    'company_id',
  ];

  public function scopeQueryAll() {
    return $this->with(['creator', 'updater'])->select(self::$showAttributes);
  }

  public function scopeQueryAllDropdown() {
    return $this->with(['company'])->select([
      'id',
      'company_id',
      'name',
    ]);
  }

  public function scopeQueryAlias() {
    return $this->with(['company'])->select([
      'id',
      'company_id',
      'alias',
    ]);
  }

  public function company() {
    return $this->belongsTo(Company::class, 'company_id')
      ->select(['id', 'name', 'alias']);
  }

  public function country() {
    return $this->belongsTo(Country::class)
      ->select(['id', 'name']);
  }

  public function city() {
    return $this->belongsTo(City::class)
      ->select(['id', 'name']);
  }
}
