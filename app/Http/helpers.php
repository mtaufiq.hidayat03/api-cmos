<?php

use Carbon\Carbon;

function getAllApprovalSppd($employee_id)
{
	$sql = "SELECT *, (a_approval ->> 'step') as step, (a_approval ->> 'approval_id') as approval_id FROM approval_sppd a
   CROSS JOIN LATERAL json_array_elements (a.approval::json) a_approval
   LEFT OUTER JOIN users us ON us.id = (a_approval ->> 'approval_id')::int
   WHERE a.user_id = '$employee_id'";

	return DB::select(DB::raw($sql));

}


function getPositionEmployee($employee_id){

	$query = DB::table('employees')
	->select('positions.name')
	->leftJoin('users', 'users.id', '=', 'employees.user_id')
	->leftJoin('positions', 'positions.id', '=', 'employees.position_id')
	->where('employees.user_id', $employee_id)
	->first();

	return $query;
}

if ( ! function_exists('dateTextMySQL2ID'))
{
    function dateTextMySQL2ID($date){
        $th=substr($date,0,4);
        $bulan=substr($date,5,2);
        $tgl=substr($date,8,2);

        $tglDepan=substr($tgl,0,1);
        $tgldiambil=substr($tgl,1,1);

        if($tglDepan=="0"){
           $tglID=$tgldiambil;
        }else{
           $tglID=$tgl;
        }

        if($bulan=="01")
        {
         $dateID ="$tglID Januari $th";
         return $dateID;
        }
        elseif($bulan=="02")
        {
         $dateID ="$tglID Februari $th";
         return $dateID;
        }
        elseif($bulan=="03")
        {
         $dateID ="$tglID Maret $th";
         return $dateID;
        }
        elseif($bulan=="04")
        {
         $dateID ="$tglID April $th";
         return $dateID;
        }
        elseif($bulan=="05")
        {
         $dateID ="$tglID Mei $th";
         return $dateID;
        }
        elseif($bulan=="06")
        {
         $dateID ="$tglID Juni $th";
         return $dateID;
        }
        elseif($bulan=="07")
        {
         $dateID ="$tglID Juli $th";
         return $dateID;
        }
        elseif($bulan=="08")
        {
         $dateID ="$tglID Agustus $th";
         return $dateID;
        }
        if($bulan=="09")
        {
         $dateID ="$tglID September $th";
         return $dateID;
        }
        elseif($bulan=="10")
        {
         $dateID ="$tglID Oktober $th";
         return $dateID;
        }
        elseif($bulan=="11")
        {
         $dateID ="$tglID November $th";
         return $dateID;
        }
        elseif($bulan=="12")
        {
         $dateID ="$tglID Desember $th";
         return $dateID;
        }
    }
}//end function

function compareLastThanOneYear($start_date) {
   $from = new Carbon($start_date);
   $to = Carbon::now();
   $diff_in_days = $to->diffInYears($from);
   if ($diff_in_days < 1) {
       return true;
   } else {
       return false;
   }
 }

 function countApprovedLeaveBalance($user_id, $expired_date, $leave_type)
 {
    $expDate = Carbon::parse($expired_date)->subYear(1)->format('Y-m-d'); // expired date - 1 tahun
    $startExpDate = Carbon::parse($expDate)->addDay(1)->format('Y-m-d'); // (expired date - 1 tahun ) + 1 hari
     $sql = "select sum(leaves.count_days) as count from leaves where leaves.user_id ='".$user_id. "' and leaves.status in (1,2,4) and leaves.created_at <='".$expired_date."' and leaves.created_at >='".$startExpDate."' and leaves.leave_type_id = '".$leave_type."' ";
     return DB::select(DB::raw($sql));
 }

 function countMassLeaveByYearAfterInOffice($startDate)
 {
   $start_date = strtotime($startDate);
   $daySt = date("d", $start_date);
   $monthSt = date("m", $start_date);
   $yearSt = Carbon::now()->format('Y');
//    $yearSt = '2020';
   $timeSt = $monthSt . "/" . $daySt . "/" . $yearSt;
   $startDate2 = strtotime($timeSt);
   $newStartDate = date('Y-m-d', $startDate2);
   $holidays = DB::table('holidays')->select('date')->whereRaw("date > '".$newStartDate."'::date")->whereRaw("date <= '2022-01-04'::date")->where('is_mass_leave', '=',  TRUE)->get();
   if($holidays) {
       $countDays = 0;
       foreach ($holidays as $holiday) {
           if (isWeekend($holiday->date)) {
               $countDays = $countDays;
           } else {
               $countDays++;
           }
       }
   }
   // return $holidays;
   return $countDays;
 }

 function isWeekend($date){
	$check = (date('N', strtotime($date)) >= 6);
    if($check){
    	return true;
    } else {
    	return false;
    }
  }

  function countLastYearOutstandingLeave($user_id, $status, $expired_date, $leave_type)
  {
    $expDate = Carbon::parse($expired_date)->subYear(1)->format('Y-m-d'); // expired date - 1 tahun
    $startExpDate = Carbon::parse($expDate)->addDay(1)->format('Y-m-d'); // (expired date - 1 tahun ) + 1 hari
    $sql = "select sum(leaves.count_days) as sum from leaves where leaves.user_id ='".$user_id. "' and leaves.status='".$status."' and leaves.created_at >= '".$startExpDate."' and leaves.created_at <= '".$expired_date."' and outstanding_leave='TRUE' and leave_type_id = '".$leave_type."'";

    return DB::select(DB::raw($sql));
  }

  function countLastYearOutstandingLeaveBeforeOneYear($user_id, $status, $start_date)
  {
    $leaveStartDate = Carbon::parse($start_date)->addYear(1)->format('Y-m-d');
    $newLeaveStartDate = Carbon::parse($leaveStartDate)->subDay(1)->format('Y-m-d');
    $sql = "select sum(leaves.count_days) as sum from leaves where leaves.user_id ='".$user_id. "' and leaves.status='".$status."' and leaves.created_at >= '".$start_date."' and leaves.created_at <='".$newLeaveStartDate."' and outstanding_leave='TRUE'";

    return DB::select(DB::raw($sql));
  }
?>