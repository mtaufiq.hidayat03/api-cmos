<?php


namespace App\Http\Controllers\Hr\ApprovalCuti;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\Hr\ApprovalECutiService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ApprovalECutiController extends Controller
{
  private $ecutiService;
  private $userRepository;

  function __construct(ApprovalECutiService $ApprovalECutiService, UserRepository $userRepository) {
    $this->ApprovalECutiService = $ApprovalECutiService;
    $this->userRepository = $userRepository;
  }

  function index() {
    $user = Auth::user();
    if ($this->userRepository->getRolesUser($user->id) === "Super Administrator") {
      return $this->ApprovalECutiService->getAllCuti();
    } else {
      return $this->ApprovalECutiService->getAllCutiByApprover($user->id);
    }
  }

  function show($id) {
    return $this->ApprovalECutiService->get($id);
  }

  function approveAction($id, $status, $user_id) {
    return $this->ApprovalECutiService->approveAction($id, $status, $user_id);
  }

}