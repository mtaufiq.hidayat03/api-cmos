<?php


namespace App\Http\Controllers\Hr;


use App\Http\Controllers\Controller;
use App\Repositories\Hr\CityRepository;
use App\Services\Hr\CityService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CityController extends Controller
{
  private $cityService;
  private $cityRepository;

  function __construct(CityService $cityService, CityRepository $cityRepository) {
    $this->cityService = $cityService;
    $this->cityRepository = $cityRepository;
  }

  function city(){
    return $this->cityService->getCity();
  }
}
