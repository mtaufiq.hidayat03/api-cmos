<?php


namespace App\Http\Controllers\Hr;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\Hr\ECutiService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ECutiController extends Controller
{
  private $ecutiService;
  private $userRepository;

  function __construct(ECutiService $ecutiService, UserRepository $userRepository) {
    $this->ecutiService = $ecutiService;
    $this->userRepository = $userRepository;
  }

  function index() {
    $user = Auth::user();
    if ($this->userRepository->getRolesUser($user->id) === "Super Administrator") {
      return $this->ecutiService->getAllCuti();
    } else {
      return $this->ecutiService->getAllCutiByUser($user->id);
    }
  }

  function store($status) {
    request()->validate([
      'leave_type_id' => 'required|exists:leave_types,id',
      'from' => 'required|date',
      'to' => 'required|date',
      'user_id' => 'numeric',
      'count_days' => 'numeric',
      'position' => 'numeric',
      'remark' => 'nullable|string',
      'status' => 'numeric',
    ]);
    
    return $this->ecutiService->create($status);
  }

  function show($id) {
    return $this->ecutiService->get($id);
  }

  function update($id, $status) {
    request()->validate([
      'leave_type_id' => 'required|exists:leave_types,id',
      'from' => 'required|date',
      'to' => 'required|date',
      'user_id' => 'numeric',
      'count_days' => 'numeric',
      'remark' => 'nullable|string',
      'status' => 'numeric',
    ]);
    return $this->ecutiService->update($id, $status);
  }

  function destroy($id) {
    return $this->ecutiService->destroy($id);
  }

  function typeLeaves() {
    return $this->ecutiService->getLeaveTypes();
  }

  function leaveBalance(){
    return $this->ecutiService->getLeaveBalance();
  }

}
