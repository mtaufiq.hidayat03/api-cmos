<?php


namespace App\Http\Controllers\Hr;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\Hr\SppdService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class SppdController extends Controller
{
  private $sppdService;
  private $userRepository;

  function __construct(SppdService $sppdService, UserRepository $userRepository) {
    $this->sppdService = $sppdService;
    $this->userRepository = $userRepository;
  }

  function index() {
    $user = Auth::user();
    if ($this->userRepository->getRolesUser($user->id) === "Super Administrator") {
      return $this->sppdService->getAllSppd();
    } else {
      return $this->sppdService->getAllSppdByUser($user->id);
    }
  }

  function sppdDetail($sppd_id){
      return $this->sppdService->getSppdById($sppd_id);
  }

  function storeSppd($status) {
    request()->validate([
      'job_place' => 'required|string',
      'date_start' => 'required|date',
      'date_end' => 'required|date',
      'description' => 'required|string',
      'description_others' => 'string',
      'contact' => 'string',
      'type' => 'nullable|numeric',
      'advance_payment_type' => 'numeric',
      'job_area' => 'numeric',
    ]);
    
    return $this->sppdService->create($status);
  }

  function storeAdvance($sppdId) {
    request()->validate([
      'item' => 'required|string',
      'price' => 'required|numeric',
    ]);
    
    return $this->sppdService->createAdvance($sppdId);
  }

  function storeHotel($sppdId) {
    request()->validate([
      'checkin' => 'required|date',
      'checkout' => 'required|date',
      'name' => 'required|string',
    ]);
    
    return $this->sppdService->createHotel($sppdId);
  }

  function storeFlight($sppdId) {
    request()->validate([
      'date' => 'required|date',
      'time' => 'required|string',
      'maskapai' => 'required|string',
      'from' => 'required|numeric',
      'to' => 'required|numeric',
    ]);
    
    return $this->sppdService->createFlight($sppdId);
  }

  function destroy($id) {
    return $this->sppdService->destroy($id);
  }

  function updateSppd($sppdId, $status, $type) {
    request()->validate([
      'job_place' => 'string',
      'date_start' => 'date',
      'date_end' => 'date',
      'description' => 'string',
      'description_others' => 'string',
      'contact' => 'string',
      'type' => 'nullable|numeric',
      'advance_payment_type' => 'numeric',
      'job_area' => 'numeric',
    ]);
    
    return $this->sppdService->updateSppd($sppdId, $status, $type);
  }

  //SPPD TICKET
  function sppdTicket($id) {
      return $this->sppdService->getAllSppdTicket($id);
  }

  function sppdTicketDestroy($id){
    return $this->sppdService->sppdTicketDestroy($id);
  }

  function sppdTicketUpdate($id) {
    request()->validate([
      'time' => 'string',
      'date' => 'date',
      'from' => 'numeric',
      'to' => 'numeric',
      'maskapai' => 'string'
    ]);
    
    return $this->sppdService->sppdTicketUpdate($id);
  }

  //SPPD Hotel
  function sppdHotel($id) {
    return $this->sppdService->getAllSppdHotel($id);
  }

  function sppdHotelDestroy($id){
    return $this->sppdService->sppdHotelDestroy($id);
  }

  function sppdHotelUpdate($id) {
    request()->validate([
      'checkin' => 'date',
      'checkout' => 'date',
      'name' => 'string'
    ]);
    
    return $this->sppdService->sppdHotelUpdate($id);
  }

  //SPPD Advances
  function sppdAdvances($id) {
    return $this->sppdService->getAllSppdAdvances($id);
  }

  function sppdAdvanceDestroy($id){
    return $this->sppdService->sppdAdvancedestroy($id);
  }

  
  function sppdAdvancesUpdate($id) {
    request()->validate([
      'item' => 'string',
      'price' => 'numeric'
    ]);
    
    return $this->sppdService->sppdAdvancesUpdate($id);
  }


  function downloadSppdPdf($id){
    return $this->sppdService->downloadSppdPdf($id);
  }
}