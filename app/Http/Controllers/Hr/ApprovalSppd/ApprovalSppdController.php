<?php


namespace App\Http\Controllers\Hr\ApprovalSppd;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\Hr\ApprovalSppdService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ApprovalSppdController extends Controller
{
  private $ecutiService;
  private $userRepository;
  private $ApprovalSppdService;

  function __construct(ApprovalSppdService $ApprovalSppdService, UserRepository $userRepository) {
    $this->ApprovalSppdService = $ApprovalSppdService;
    $this->userRepository = $userRepository;
  }

  function index() {
    $user = Auth::user();
    if ($this->userRepository->getRolesUser($user->id) === "Super Administrator") {
      return $this->ApprovalSppdService->getAllSppd();
    } else {
      return $this->ApprovalSppdService->getAllSppdByApprover($user->id);
    }
  }

  function show($id) {
    return $this->ApprovalSppdService->get($id);
  }

  function approveAction($id, $status, $user_id) {
    return $this->ApprovalSppdService->approveAction($id, $status, $user_id);
  }

}