<?php

namespace App\Http\Controllers;

use App\Services\AnnouncementService;

class AnnouncementController extends Controller {
  private $announcementService;

  function __construct(AnnouncementService $announcementService) {
    $this->announcementService = $announcementService;
  }

  function index() {
    return $this->announcementService->getAll();
  }

  function show($id) {
    return $this->announcementService->get($id);
  }

  function update($id) {
    request()->validate([]);
    return $this->announcementService->update($id);
  }

  function destroy($id) {
    return $this->announcementService->destroy($id);
  }
}
