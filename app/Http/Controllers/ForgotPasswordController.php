<?php

namespace App\Http\Controllers;

use App\Services\ForgotPasswordService;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Contracts\Auth\PasswordBroker;


class ForgotPasswordController extends Controller {
  private $forgotPasswordService;

  function __construct(ForgotPasswordService $forgotPasswordService) {
    $this->forgotPasswordService = $forgotPasswordService;
  }

  public function forgotPassword() {
    request()->validate([
      'email' => 'required|exists:users,email',
    ]);
    return $this->forgotPasswordService->forgotPassword();
  }

  public function updatePassword() {
    request()->validate([
      'token' => 'required',
      'email' => 'required|email|exists:users,email',
      'password' => 'required|string'
    ]);
    return $this->forgotPasswordService->updatePassword();
  }
}
