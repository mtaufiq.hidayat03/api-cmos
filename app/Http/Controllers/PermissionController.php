<?php

namespace App\Http\Controllers;

use App\Permission;

class PermissionController extends Controller {
  public function __construct() {
  }
  public function index() {
    $permissions = collect(Permission::get())->map(function ($obj) {
      return $obj->name;
    });
    return $this->getResponse($permissions);
  }
}
