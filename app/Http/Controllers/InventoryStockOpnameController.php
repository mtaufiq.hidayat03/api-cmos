<?php

namespace App\Http\Controllers;

use App\Services\InventoryStockOpnameService;

class InventoryStockOpnameController extends Controller {
  private $inventoryStockOpnameService;

  function __construct(InventoryStockOpnameService $inventoryStockOpnameService) {
    $this->inventoryStockOpnameService = $inventoryStockOpnameService;
  }

  function index() {
    return $this->inventoryStockOpnameService->getAll();
  }

  function show($id) {
    return $this->inventoryStockOpnameService->get($id);
  }

  function store() {
    request()->validate([
      'inventory_id' => 'required|exists:inventories,id',
      'good_stock' => 'required|numeric',
      'bad_stock' => 'required|numeric',
      'notes' => 'nullable|string',
    ]);
    return $this->inventoryStockOpnameService->create();
  }

  function qrCode($uuid) {
    return $this->inventoryStockOpnameService->qrCode($uuid);
  }

  function update($id) {
    request()->validate([]);
    return $this->inventoryStockOpnameService->update($id);
  }

  function destroy($id) {
    return $this->inventoryStockOpnameService->destroy($id);
  }

  function exportExcel(){
    return $this->inventoryStockOpnameService->exportExcel();
  }
}
