<?php

namespace App\Http\Controllers;

use App\Role;
use App\Setting;
use App\Services\RoleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\PermissionRegistrar;

class RoleController extends Controller {
  private $roleService;

  public function __construct(RoleService $roleService) {
    $this->roleService = $roleService;
  }

  public function index() {
    return $this->roleService->index();
  }

  public function show($id) {
    return $this->roleService->show($id);
  }
}
