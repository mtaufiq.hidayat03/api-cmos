<?php

namespace App\Http\Controllers;

use App\Services\LocationService;

class LocationController extends Controller {
  private $locationService;

  function __construct(LocationService $locationService) {
    $this->locationService = $locationService;
  }

  function index() {
    return $this->locationService->getAll();
  }

  function dropdown() {
    return $this->locationService->getAllDropdown();
  }

  function show($id) {
    return $this->locationService->get($id);
  }

  function qrCode($uuid) {
    return $this->locationService->qrCode($uuid);
  }

  function update($id) {
    request()->validate([]);
    return $this->locationService->update($id);
  }

  function destroy($id) {
    return $this->locationService->destroy($id);
  }
}
