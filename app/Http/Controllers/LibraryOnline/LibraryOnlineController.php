<?php


namespace App\Http\Controllers\LibraryOnline;


use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Services\LibraryOnline\LibraryOnlineService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class LibraryOnlineController extends Controller
{
  private $libraryService;
  private $userRepository;

  function __construct(LibraryOnlineService $libraryService, UserRepository $userRepository) {
    $this->libraryService = $libraryService;
    $this->userRepository = $userRepository;
  }

  function index() {
    $user = Auth::user();
    // return $this->libraryService->getAllLibrary();
    if ($user->type === 1 || $user->type === 10) {
      return $this->libraryService->getAllLibrary();
    } else {
      return $this->libraryService->getAllLibraryByUser($user->id);
    }
  }

  function show($id){
    return $this->libraryService->getLibraryById($id);
  }

  function downloadLibrary($id){
    return $this->libraryService->downloadLibraryOnlineById($id);
  }

  function history($id) {
      return $this->libraryService->getAllLibraryHistory($id);
  }
}