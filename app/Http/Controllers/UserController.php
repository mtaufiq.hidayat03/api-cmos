<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller {
  private $userService;

  function __construct(UserService $userService) {
    $this->userService = $userService;
  }

  function index() {
    return $this->userService->getAll();
  }

  function show($id) {
    return $this->userService->get($id);
  }

  function update($id) {
    request()->validate([
      'username' => 'unique:users,username,NULL,id,deleted_at,NULL',
      'email' => 'email|unique:users,email,NULL,id,deleted_at,NULL',
      'gender' => [
        Rule::in(['male', 'female']),
      ],
    ]);

    return $this->userService->update($id);
  }

  function destroy($id) {
    return $this->userService->destroy($id);
  }
}
