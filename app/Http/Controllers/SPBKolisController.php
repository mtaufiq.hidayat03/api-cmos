<?php

namespace App\Http\Controllers;

use App\Services\SPBKolisService;

class SPBKolisController extends Controller {
  private $spbKolisService;

  function __construct(SPBKolisService $spbKolisService) {
    $this->spbKolisService = $spbKolisService;
  }

  function index() {
    return $this->spbKolisService->getAll();
  }
  
  function qrCode($uuid) {
    return $this->spbKolisService->qrCode($uuid);
  }

  function store() {
    request()->validate([
      'spb_id' => 'required|exists:spb,id',
      'location_id' => 'required|exists:locations,id',
      'received_by' => 'required',
      'spb_items' => 'required|array',
      'spb_items.*.spb_kolis_id' => 'required|exists:spb_kolis,id',
      'spb_items.*.qty' => 'required',
    ]);
    return $this->spbKolisService->store();
  }
}
