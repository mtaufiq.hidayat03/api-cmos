<?php

namespace App\Http\Controllers;

use App\Services\InventoryService;

class InventoryController extends Controller {
  private $inventoryService;

  function __construct(InventoryService $inventoryService) {
    $this->inventoryService = $inventoryService;
  }

  function index() {
    return $this->inventoryService->getAll();
  }

  function show($id) {
    return $this->inventoryService->get($id);
  }

  function qrCode($uuid) {
    return $this->inventoryService->qrCode($uuid);
  }

  function update($id) {
    request()->validate([]);
    return $this->inventoryService->update($id);
  }

  function destroy($id) {
    return $this->inventoryService->destroy($id);
  }
}
