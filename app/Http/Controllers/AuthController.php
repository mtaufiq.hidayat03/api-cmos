<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use App\User;
use App\Setting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

class AuthController extends Controller {
  private $authService;

  public function __construct(AuthService $authService) {
    $this->authService = $authService;
  }

  public function login() {
    request()->validate([
      'email' => 'required|string|email',
      'password' => 'required|string',
    ]);
    return $this->authService->login();
  }

  public function register() {
    request()->validate([
      'email' => 'required|email|unique:users,email,NULL,id',
      'password' => 'required|string',
      'name' => 'required|string',
      'roles' => 'array',
      'roles.*' => 'string|distinct|exists:roles,name',
      'company_id' => 'numeric|exists:companies,id',
      'location_id' => 'numeric|exists:locations,id',
      'notification_email' => 'boolean',
    ]);
    return $this->authService->register();
  }

  public function logout() {
    return $this->authService->logout();
  }

  public function profile() {
    return $this->authService->profile();
  }

  public function changePassword() {
    return $this->authService->changePassword();
  }
}
