<?php

namespace App\Spatie\Filters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class InventoryStockOpnameFilter implements \Spatie\QueryBuilder\Filters\Filter {
  public function __construct() {
  }
  public function __invoke(Builder $query, $value, string $property) {
    $query->whereBetween($property, [date($value[0]), date($value[1])]);
    return $query;
  }
}
