<?php

namespace App;

use App\Location;

class Inventory extends BaseModel {
  protected $casts = [
    'status' => 'boolean',
  ];
  public static $showAttributes = [
    'id',
    'stock_onhand',
    'stock_min',
    'stock_max',
    'code_rack',
    'product_type',
    'initial',
    'stock_status',
    'in',
    'out',
    'kind',
    'price',
    'uuid',
    'created_at',
    'created_by',
    'product_id',
    'location_id',
    'is_local'
  ];

  public function scopeQueryAll() {
    return $this->with(['creator', 'product', 'location','inventoryStockOpname'])->select(self::$showAttributes);
  }

  public function scopeQueryDetail() {
    return $this->with(['creator', 'product', 'location','inventoryStockOpname'])
      ->whereIsLocal(false)
      ->select(self::$showAttributes);
  }

  public function location() {
    return $this->belongsTo(Location::class, 'location_id')
      ->select(Location::$showAttributes)
      ->with(['company', 'country', 'city']);
  }

  public function product() {
    return $this->belongsTo(Product::class, 'product_id')
      ->select(Product::$showAttributes)
      ->with(['creator',  'category', 'measure', 'item']);
  }

  public function inventoryStockOpname() {
    return $this->hasOne(InventoryStockOpname::class)
      ->select(InventoryStockOpname::$showAttributes)
      ->latest();
  }
}
