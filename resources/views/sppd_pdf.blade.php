


<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style type="text/css">
        table{
            width:100%;
        }
        body{
            font-size: 10pt;
        }

        table.table-bordered {
            font-size: 9pt;
            border-left: 0.01em solid #ccc;
            border-right: 0;
            border-top: 0.01em solid #ccc;
            border-bottom: 0;
            border-collapse: collapse;
        }
        table.table-bordered td,
        table.table-bordered th {
            border-left: 0;
            border-right: 0.01em solid #ccc;
            border-top: 0;
            border-bottom: 0.01em solid #ccc;
            padding:5px 8px;
        }

        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 1px;
            padding:5px;
        }
        .text-center{
            text-align:center;
        }
        .text-uppercase{
            text-transform:uppercase;
        }
        .page-break {
            page-break-after: always;
        }
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-baqh{text-align:center;vertical-align:top}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        .tg .tg-0lax{text-align:left;vertical-align:top}

        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}

        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
        font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        .tg .tg-0lax{text-align:left;vertical-align:top}
            </style>
	
	@yield('css')

</head>

<div class="mB-40" style="margin: 40px; margin-left:30px; margin-right:30px;">
	<div class="p-30">
		<table class="table border-0">
			<tr>
				<td class="border-0 text-center" style="width:100%"> 
					<span class="text-bold" style="font-weight:bold; font-size: 25px; "> PT CITA MINERAL INVESTINDO Tbk </span> <br>
					<span class="text-bold" style="font-size: 12px;"> Panin Bank Building Lantai 2, Jl. Jend. Sudirman - Senayan, Jakarta Pusat 102720 </span> <br>
                    <span class="text-bold" style="font-size: 12px;"> Telp. (021) - 7251344 Fax (021) - 72789885 </span> <br>
				</td>
			</tr>
		</table>
        <hr>
        <br>
		<table class="table border-0">
			<tr>
				<td class="border-0 text-center" style="width:100%"> 
					<span class="text-bold" style="font-weight:bold; font-size: 17px; "> SURAT PERINTAH PERJALANAN DINAS </span> <br>
                    <span class="text-bold" style="font-size: 14px; "> NOMOR : {{ $sppd->doc_no }} </span> <br>
				</td>
			</tr>
		</table>

        <br>
        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Direksi PT. Cita Mineral Investindo, Tbk menugaskan:</span> <br>
				</td>
			</tr>
		</table>

        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:30%"> 
                    <span class="text-bold" style="font-size: 12px; "> Nama </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Nomor Induk Karyawan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Jabatan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Golongan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Maksud Kedatangan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Tempat Penugasan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Daerah Penugasan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Tanggal Berangkat </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Tanggal Kembali </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Beban Biaya </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> No. HP yang bisa dihubungi </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Lain-lain </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Jenis SPPD </span> <br>
				</td>
                <td class="border-0" style="width:70%"> 
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->employeeName)) }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->employeeNik }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->position)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->group }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->description)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->job_place)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($city)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->date_start }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->date_end }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->budget_by)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->contact }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->description_others)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : @if ($sppd->type=='1')
                        Dalam Negeri
                    @else
                        Luar Negeri
                    @endif</span> <br>
				</td>
			</tr>
		</table>

        <br>
        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Demikian Surat Perintah Perjalanan Dinas ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</span> <br>
                    <br><br><br><br>
                    <span class="text-bold" style="font-size: 12px; "> Dikeluarkan di: Jakarta</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Pada Tanggal : {{ dateTextMySQL2ID($sppd->created_at) }}</span> <br>
                    <span class="text-bold" style="font-weight: bold; font-size: 14px; "> Ferry Kadi</span> <br>
                    <span class="text-bold" style="font-weight: bold; font-size: 14px; "> Direktur</span> <br>
				</td>
			</tr>
		</table>
        <br><br>
        <table class="tg text-center">
            <thead>
              <tr>
                <th class="tg-0lax" colspan="2">Tiba di</th>
                <th class="tg-0lax" colspan="2">Meninggalkan</th>
                <th class="tg-0lax" rowspan="2">Tiba di Jakarta</th>
              </tr>
              <tr>
                <th class="tg-0lax">Kantor Cabang /Mess</th>
                <th class="tg-0lax">Site</th>
                <th class="tg-0lax">Kantor Cabang /Mess</th>
                <th class="tg-0lax">Site</th>
              </tr>
            </thead>
            <tbody>
              <tr style="height: 150px"> 
                <td class="tg-0lax"><br><br><br><br><br></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
              </tr>
              <tr>
                <td class="tg-0lax">Pengurus Cabang</td>
                <td class="tg-0lax">Site Manager/KoorLap</td>
                <td class="tg-0lax">Pengurus Cabang</td>
                <td class="tg-0lax">Site Manager/KoorLap</td>
                <td class="tg-0lax">HRD</td>
              </tr>
            </tbody>
            </table>

            <br>
            <table class="table border-0">
                <tr>
                    <td class="border-0" style="width:100%"> 
                        <span class="text-bold" style="font-weight: bold; font-size: 12px; "> Note :</span> <br>
                        <span class="text-bold" style="font-size: 12px; "> - Mohon diisi setiap kolom diatas pada saat dan akan meninggalkan lokasi/Site </span> <br>
                        <span class="text-bold" style="font-size: 12px; "> - Setelah kembali ke Jakarta, Surat Tugas ini dilaporkan kembali kepada HRD Jakarta</span> <br>
                </tr>
            </table>

        
		<table class="table border-0">
			
			<tbody>
				
			</tbody>
		</table>

        

	</div>
</div>
<br><br>
<div class="page-break">
    <div class="p-30">
        <table class="table border-0">
            <tr>
                <td class="border-0 text-center" style="width:100%"> 
                    <span class="text-bold" style="font-weight:bold; font-size: 17px; "> SURAT PENUGASAN & PROPOSAL ADVANCE PERJALANAN DINAS </span> <br>
                </td>
            </tr>
        </table>
        <hr>
        <br><br><br>
        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Dengan ini ditugaskan Kepada:</span> <br>
				</td>
			</tr>
		</table>

        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:20%"> 
                    <span class="text-bold" style="font-size: 12px; "> Nama </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Nomor Induk Karyawan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Jabatan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Golongan </span> <br>
                    
                    
                    
                    <span class="text-bold" style="font-size: 12px; "> Tanggal Penugasan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Tempat Penugasan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Maksud Kedatangan </span> <br>
				</td>
                <td class="border-0" style="width:70%"> 
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->employeeName)) }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->employeeNik }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->position)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->group }}</span> <br>
                    
                    
                    
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->date_start }} - {{ $sppd->date_end }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->job_place)) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->description)) }}</span> <br>
				</td>
			</tr>
		</table>
        <br><br>
        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Perincian Rencana Pengeluaran:</span> <br>
				</td>
			</tr>
		</table>
        <br>
        <table class="tg">
            <thead>
              <tr>
                <th class="tg-0pky" rowspan="1">No</th>
                <th class="tg-0lax" rowspan="1">Keterangan</th>
                <th class="tg-baqh" colspan="3">Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax">Diusulkan</td>
                <td class="tg-0lax">Revisi Controller</td>
                <td class="tg-0lax">Disetujui</td>
              </tr>
              @php
                $no = 1;
                $priceTotal = 0;
              @endphp
              @foreach ($sppd_advance as $sa)
              <tr>
                <td class="tg-0lax">{{ $no }}</td>
                <td class="tg-0lax">{{ ucwords(strtolower($sa->item)) }}</td>
                <td class="tg-0lax">{{ number_format($sa->price,0,",",".") }}</td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
              </tr>

              @php
                  $no++;
                  $priceTotal += $sa->price;
              @endphp
              @endforeach
              <tr>
                <td class="tg-0lax" colspan="2">Total yang diajukan (Rp)</td>
                <td class="tg-0lax">{{ number_format($priceTotal,0,",",".") }}</td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
              </tr>
            </tbody>
            </table>
    
            <br><br>
            <table class="table border-0">
                <tr>
                    <td class="border-0" style="width:100%"> 
                        <span class="text-bold" style="font-size: 12px; "> Penyerahan Advance:</span> <br>
                    </td>
                </tr>
            </table>
            <br>
            <table class="tg">
                <thead>
                  <tr>
                    <th class="tg-0pky">No</th>
                    <th class="tg-c3ow" colspan="2">Keterangan</th>
                    <th class="tg-0pky">Diserahkan Oleh</th>
                    <th class="tg-0pky">Diterima Oleh</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        if ($sppd->advance_payment_type=='2'){
                            $bank_transfer = 'Yes';
                            $tunai = 'No';
                        }else{
                            $bank_transfer = 'No';
                            $tunai = 'Yes';
                        }
                    @endphp
                  <tr>
                    <td class="tg-0pky">1</td>
                    <td class="tg-0pky">Tunai *)</td>
                    <td class="tg-0pky">{{ $tunai }}</td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky">2</td>
                    <td class="tg-0pky">Bank Transfer **)</td>
                    <td class="tg-0pky">{{ $bank_transfer }}</td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Nama Bank</td>
                    <td class="tg-0pky">
                        @if ($sppd->advance_payment_type == 2)
                            {!! getDataById('banks',$sppd->bank)->name !!}
                        @else
                            -
                        @endif
                    </td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Cabang</td>
                    <td class="tg-0pky">
                        @if ($sppd->advance_payment_type == 2)
                            {{ $sppd->bankBranch }}
                        @else
                            -
                        @endif
                    </td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Nomor Rekening</td>
                    <td class="tg-0pky">
                        @if ($sppd->advance_payment_type == 2)
                            {{ $sppd->bankNumber }}
                        @else
                            -
                        @endif
                    </td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Atas Nama</td>
                    <td class="tg-0pky">
                        @if ($sppd->advance_payment_type == 2)
                            {{ $sppd->bankAccount }}
                        @else
                            -
                        @endif
                    </td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Tanggal Transfer</td>
                    <td class="tg-0pky">
                        @if ($sppd->advance_payment_type == 2)
                            {{ $sppd->advance_date_transfer }}
                        @else
                            -
                        @endif
                    </td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky"></td>
                  </tr>
                  <tr>
                    <td class="tg-0pky" colspan="2"></td>
                    <td class="tg-0pky"></td>
                    <td class="tg-0pky">Finance</td>
                    <td class="tg-0pky">Karyawan/ti</td>
                  </tr>
                </tbody>
                </table>

                

    </div>
</div>

<div class="page-break">
    <div class="p-30">
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Status Advance Periode Sebelumnya:</span> <br>
                </td>
            </tr>
        </table>
        <br>
        <table class="tg">
            <thead>
            <tr>
                <th class="tg-0pky" rowspan="2">No</th>
                <th class="tg-0pky" colspan="2">Proposal Advance</th>
                <th class="tg-0pky" rowspan="2">Nomor SPPD</th>
                <th class="tg-c3ow" colspan="5">Status Advance ***)</th>
            </tr>
            <tr>
                <th class="tg-0pky"></th>
                <th class="tg-0pky">Tanggal</th>
                <th class="tg-0pky">Nomor</th>
                <th class="tg-0pky">Selesai</th>
                <th class="tg-0lax">Paraf</th>
                <th class="tg-0lax">Dalam Proses</th>
                <th class="tg-0lax">Outstanding</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="tg-0pky"><br><br><br><br><br></td>
                <td class="tg-0pky"></td>
                <td class="tg-0pky"></td>
                <td class="tg-0pky"></td>
                <td class="tg-0pky"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
                <td class="tg-0lax"></td>
            </tr>
            </tbody>
        </table>
        <br><br>

        @php
            $getRule = getAllApprovalSppd($sppd->user_id);
        @endphp
        <table class="tg">
            <thead>
            <tr>
                <th class="tg-0pky">Mengajukan,</th>
                <th class="tg-0pky" colspan="{{ count($getRule) }}">Menyetujui,</th>
                <th class="tg-0pky">Dikeluarkan Oleh,</th>
            </tr>
            <tr>
                <th class="tg-0pky"><br><br><br></th>
                @foreach ($getRule as $item)
                <th class="tg-0pky"></th>
                @endforeach
                <th class="tg-0pky"></th>
            </tr>
            </thead>
            <tbody>
                @php
                    
                @endphp
            <tr>
                <td class="tg-0pky">
                    {{ ucwords(strtolower($sppd->employeeName)) }} <br>
                    {{ ucwords(strtolower($sppd->position)) }}
                
                </td>
                @foreach ($getRule as $item)
                    <td class="tg-0pky">
                        {{ ucwords(strtolower($item->name)) }} <br>
                        {!! ucwords(strtolower(getPositionEmployee($item->approval_id)->name)) !!}
                        {{-- Approval {{ $item->step }}    --}}
                    </td>    
                @endforeach
                <td class="tg-0pky">Finance</td>
            </tr>
            </tbody>
        </table>
        
        <br><br>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Laporan Realisasi Perjalanan Dinas:</span> <br>
                </td>
            </tr>
        </table>
        <br>
        <table class="tg">
            
            <tbody>
            <tr>
                <td style="width: 50px">Nomor </td>
                <td style="width: 50px"></td>
                
            </tr>
            <tr>
                <td style="width: 50px">Tanggal</td>
                <td style="width: 50px"></td>
                
            </tr>
            </tbody>
        </table>

        <br><br>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 14px; font-weight: bold;"> Catatan:</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> *) Untuk menghindari lonjakan harga tiket pesawat karena peak season mohon segera memastikan tgl. kepulangan/cuti roaster berikutnya.</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> **) Dilingkari salah satu sesuai dengan cara penyerahan advance.</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> ***) Diisi paraf Staff HRD pada kolom yang sesuai.</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Lembar ke-1 ditujukan ke Finance untuk pengajuan Advance</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Lembar copy disimpan bagaian HRD untuk dilampirkan pada Form Realiasi Biaya Perjalanan Dinas pada saat pertanggungjawaban advance.</span> <br>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="page-break">
    <div class="p-30">

        <table class="table border-0">
            <tr>
                <td class="border-0 text-center" style="width:100%"> 
                    <span class="text-bold" style="font-weight:bold; font-size: 17px; "> FORM PEMESANAN TIKET PESAWAT DAN HOTEL </span> <br>
                </td>
            </tr>
        </table>
        <br>
        <br><br>

        <table class="table border-0">
			<tr>
				<td class="border-0" style="width:20%"> 
                    <span class="text-bold" style="font-size: 12px; "> Nama Karyawan</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Nomor Induk Karyawan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Jabatan / Golongan </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Maksud Kedatangan </span> <br>
				</td>
                <td class="border-0" style="width:70%"> 
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->employeeName)) }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ $sppd->employeeNik }} </span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->position)) }} - {{ $sppd->group }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> : {{ ucwords(strtolower($sppd->description)) }}</span> <br>
				</td>
			</tr>
		</table>
        <br><br>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Daftar Tiket Pesawat:</span> <br>
                </td>
            </tr>
        </table>
        <br>
        <table class="tg">
            <thead>
              <tr>
                <th class="tg-0lax">No</th>
                <th class="tg-0lax">Tanggal Flight</th>
                <th class="tg-0lax">Dari - Ke</th>
                <th class="tg-0lax">Maskapai</th>
                <th class="tg-0lax">Keterangan</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $no=1;
                @endphp
                @foreach ($sppd_flight as $sf)
              <tr>
                <td class="tg-0lax">{{ $no }}</td>
                <td class="tg-0lax">{{ dateTextMySQL2ID($sf->date) }}</td>
                <td class="tg-0lax">{{ ucwords(strtolower($sf->cityFrom))." - ".ucwords(strtolower($sf->cityTo)) }}</td>
                <td class="tg-0lax">{{ ucwords(strtolower($sf->maskapai)) }}</td>
                <td class="tg-0lax">{{ $sf->time }}</td>
              </tr>
              @php
                  $no++;
              @endphp
              @endforeach
            </tbody>
        </table>

        <br><br>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Daftar Tiket Hotel:</span> <br>
                </td>
            </tr>
        </table>
        <br>
        <table class="tg">
            <thead>
              <tr>
                <th class="tg-0lax">No</th>
                <th class="tg-0lax">Hotel</th>
                <th class="tg-0lax">Check In</th>
                <th class="tg-0lax">Check Out</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $no=1;
                @endphp
                @foreach ($sppd_hotel as $sh)
                    <tr>
                        <td class="tg-0lax">{{ $no }}</td>
                        <td class="tg-0lax">{{ ucwords(strtolower($sh->name)) }}</td>
                        <td class="tg-0lax">{{ dateTextMySQL2ID($sh->checkin) }}</td>
                        <td class="tg-0lax">{{ dateTextMySQL2ID($sh->checkout) }}</td>
                    </tr>
                @php
                    $no++;
                @endphp
              @endforeach
            </tbody>
        </table>
        <br><br>
        <hr>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 14px; font-weight: bold;"> Note:</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> - Kesalahan penentuan tanggal, jam dan rute Keberangkatan maupun kembali akibat kekeliruan dari pemesanan, maka segala biaya-biaya yang ditimbulkan menjadi tanggung jawab pemesan</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> - Perubahan rute, jam, tanggal, akibat dari kebijakan pimpinan/kegiatan perusahaan, harus mendapat Surat Keterangan tertulis dari pimpinan terkait</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> - Untuk tiket kembali apabila belum dipesan maka dapat dipesan melalui HRD setempat (Site)</span> <br>
                </td>
            </tr>
        </table>
        <br><br>
        <hr>
        <br>
        <table class="table border-0">
            <tr>
                <td class="border-0" style="width:100%"> 
                    <span class="text-bold" style="font-size: 12px; "> Jakarta, {{ dateTextMySQL2ID($sppd->created_at) }}</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> Pemesan</span> <br>
                    <br><br><br>
                    <span class="text-bold" style="font-size: 12px; "> ( {{ ucwords(strtolower($sppd->employeeName)) }} )</span> <br>
                    <br><br>
                    <span class="text-bold" style="font-size: 12px; "> Kelengkapan :</span> <br>
                    <span class="text-bold" style="font-size: 12px; "> - SPPD / Proposal Advance</span> <br>
                </td>
            </tr>
        </table>

    </div>
</div>
	
</body>

</html>