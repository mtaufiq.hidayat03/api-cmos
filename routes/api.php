<?php

use Illuminate\Support\Facades\Route;

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('forgot_password', 'ForgotPasswordController@forgotPassword');
Route::post('update_password', 'ForgotPasswordController@updatePassword')->name('password.reset');

Route::group(['middleware' => ['auth.httponly']], function () {
  // Authentication
  Route::post('logout', 'AuthController@logout');
  Route::get('profile', 'AuthController@profile');
  Route::post('change_password', 'AuthController@changePassword');

  Route::get('roles', 'RoleController@index');
  Route::get('roles/{id}', 'RoleController@show');

  // Announcements
  Route::get('announcements', 'AnnouncementController@index');

  // Invetories
  Route::get('inventories', 'InventoryController@index');
  Route::get('inventories_qr/{uuid}', 'InventoryController@qrCode');
  // Invetory Stock Opname
  Route::get('inventory_stock_opnames', 'InventoryStockOpnameController@index');
  Route::get('inventory_stock_opnames_excel', 'InventoryStockOpnameController@exportExcel');
  Route::get('inventory_stock_opnames_qr/{uuid}', 'InventoryStockOpnameController@qrCode');
  Route::post('inventory_stock_opnames', 'InventoryStockOpnameController@store');

  // Dropdown location
  Route::get('locations', 'LocationController@index');
  Route::get('locations_dropdown', 'LocationController@dropdown');

  // SPBKolis
  Route::get('spb_kolises_qr/{uuid}', 'SPBKolisController@qrCode');
  Route::post('spb_kolises', 'SPBKolisController@store');

  //E-Cuti
  Route::get('ecuti_list', 'Hr\ECutiController@index');
  Route::post('ecuti_store/{status}', 'Hr\ECutiController@store');
  Route::get('leave_type_list', 'Hr\ECutiController@typeLeaves');
  Route::get('ecuti_edit/{id}', 'Hr\ECutiController@show');
  Route::post('ecuti_update/{id}/{status}', 'Hr\ECutiController@update');
  Route::delete('ecuti_delete/{id}', 'Hr\ECutiController@destroy');
  Route::get('leave_balance', 'Hr\ECutiController@leaveBalance');

  Route::get('city_list', 'Hr\CityController@city');

  //SPPD
  Route::get('sppd_list', 'Hr\SppdController@index');
  Route::get('sppd_detail/{sppd_id}', 'Hr\SppdController@sppdDetail');
  Route::post('sppd_store/{status}', 'Hr\SppdController@storeSppd');
  Route::delete('sppd_delete/{id}', 'Hr\SppdController@destroy');
  Route::post('sppd_update/{id}/{status}/{type}', 'Hr\SppdController@updateSppd');
  Route::get('sppd_pdf/{id}', 'Hr\SppdController@downloadSppdPdf');

  //SPPD => ticket, advance, hotel
  Route::get('sppd_ticket/{sppd_id}', 'Hr\SppdController@sppdTicket');
  Route::get('sppd_advances/{sppd_id}', 'Hr\SppdController@sppdAdvances');
  Route::get('sppd_hotel/{sppd_id}', 'Hr\SppdController@sppdHotel');
  
  Route::post('sppd_advance_store/{sppd_id}', 'Hr\SppdController@storeAdvance');
  Route::post('sppd_flight_store/{sppd_id}', 'Hr\SppdController@storeFlight');
  Route::post('sppd_hotel_store/{sppd_id}', 'Hr\SppdController@storeHotel');

  Route::delete('sppd_ticket_delete/{id}', 'Hr\SppdController@sppdTicketDestroy');
  Route::delete('sppd_advances_delete/{id}', 'Hr\SppdController@sppdAdvanceDestroy');
  Route::delete('sppd_hotel_delete/{id}', 'Hr\SppdController@sppdHotelDestroy');

  Route::put('sppd_ticket_update/{id}', 'Hr\SppdController@sppdTicketUpdate');
  Route::put('sppd_advances_update/{id}', 'Hr\SppdController@sppdAdvancesUpdate');
  Route::put('sppd_hotel_update/{id}', 'Hr\SppdController@sppdHotelUpdate');


  //Approval E-Cuti
  Route::get('approval_cuti_list', 'Hr\ApprovalCuti\ApprovalECutiController@index');
  Route::get('approval_cuti_detail/{id}', 'Hr\ApprovalCuti\ApprovalECutiController@show');
  Route::post('approval_act/{id}/{status}/{userid}', 'Hr\ApprovalCuti\ApprovalECutiController@approveAction');

  //Approval SPPD
  Route::get('approval_sppd_list', 'Hr\ApprovalSppd\ApprovalSppdController@index');
  Route::get('approval_cuti_detail/{id}', 'Hr\ApprovalSppd\ApprovalSppdController@show');
  Route::post('approval_sppd_act/{id}/{status}/{userid}', 'Hr\ApprovalSppd\ApprovalSppdController@approveAction');

  //Library Online
  Route::get('library_online', 'LibraryOnline\LibraryOnlineController@index');
  Route::get('library_online_detail/{id}', 'LibraryOnline\LibraryOnlineController@show');
  Route::get('library_online_download/{id}', 'LibraryOnline\LibraryOnlineController@downloadLibrary');
  Route::get('library_online_history/{id}', 'LibraryOnline\LibraryOnlineController@history');
  Route::get('library_online_history_detail/{id}', 'LibraryOnline\LibraryOnlineController@historyDetail');



  //Absensi

});
